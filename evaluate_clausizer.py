import os
import spacy
import sys

from pipeline.components.clausizer import dependency_clausizer


def get_tags(tagstr):
    """Transform a gold label into a list of gold tags,
        e.g. "*S)S)" -> ["*", "S)", "S)"]
    
    Args:
        tagstr (str): The gold label for a token.
    
    Returns:
        list of str: All gold tags for the token.
    
    """
    tags = []
    while tagstr != "":
        if tagstr.startswith("*"):
            tags.append(tagstr[0:1])
            tagstr = tagstr[1:]
        elif tagstr.startswith("(S") or tagstr.startswith("S)"):
            tags.append(tagstr[0:2])
            tagstr = tagstr[2:]
    return tags


# load test corpus
with open(os.path.join("clauses", "data", "testb3"), "r") as f:
    tokens, pos_tags, sent_starts, gold_tags = [], [], [], []
    sent_start = True
    for line in f:
        line = line.strip()
        if line == "":
            sent_start = True
        else:
            line = line.split()
            if line[0] == "COMMA":
                line[0] = ","
            tokens.append(line[0])
            pos_tags.append(line[1])
            sent_starts.append(sent_start)
            gold_tags.append(get_tags(line[-1]))
            sent_start = False


def set_tags_and_sentences(doc, pos_tags, sent_starts):
    """Spacy pipeline component.
        Add POS tags and sentence boundaries to the document.

    Args:
        doc (`Doc`): A spacy document object.
        pos_tags (list of str): POS tag for every token.
        sent_start (list of boolean): Truth value for every token;
            True iff the token is the first token of a sentence.
    
    Returns:
        `Doc`: A spacy document object.

    """
    for i, pos_tag in enumerate(pos_tags):
        doc[i].tag_ = pos_tag
        doc[i].is_sent_start = sent_starts[i]
    return doc


# load UD spacy model
sp = spacy.load("en_core_web_lg", disable=["tagger"])
sp.tokenizer = sp.tokenizer.tokens_from_list

# add components
sp.add_pipe(lambda doc: set_tags_and_sentences(doc, pos_tags, sent_starts), name="sentencizer", before="parser")
sp.add_pipe(dependency_clausizer, name="clausizer")

# create SpaCy document
doc = sp(tokens)


def evaluate_clauses(doc, tag):
    """Clause evaluation.

    Args:
        doc (`Doc`): Spacy document.
        tag (str): "(S" to evaluate clause starts; "S)" to evaluate clause ends.
    
    Returns:
        float: Precision of the clause start/end detection.
        float: Recall of the clause start/end detection.
        float: F-Score of the clause start/end detection.
    
    """
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    sout = sys.stdout
    sys.stdout = open(os.path.join("evaluate_tense_tagger_logs", "clauses.tsv"),'wt')
    for sent in doc.sents:
        for token in sent:
            tags = ["*"]
            for i, clause in enumerate(sent._.clauses):
                clause_tokens = clause._.prec_punct + clause._.tokens + clause._.succ_punct
                if token not in clause_tokens:
                    continue
                if clause_tokens.index(token) == 0:
                    tags.insert(0, "(S")
                if clause_tokens.index(token) == len(clause_tokens)-1:
                    tags.append("S)")
            print("\t".join([token.text, token.tag_, "".join(gold_tags[token.i]), "".join(tags)]))
            gold = gold_tags[token.i].count(tag) > 0
            pred = tags.count(tag) > 0
            if gold:
                if pred:
                    tp += 1
                else:
                    fn += 1
            else:
                if pred:
                    fp += 1
                else:
                    tn += 1
        print()
    precision = 1.0*tp/(tp+fp)
    recall = 1.0*tp/(tp+fn)
    fscore = 2.0*precision*recall/(precision+recall)
    sys.stdout = sout
    print("Gold:", tp+fn)
    print("Pred:", tp+fp)
    return precision, recall, fscore


print("Clauses (start)")
p, r, f = evaluate_clauses(doc, "(S")
print("Pr:", p)
print("Re:", r)
print("F1:", f)

print("Clauses (end)")
p, r, f = evaluate_clauses(doc, "S)")
print("Pr:", p)
print("Re:", r)
print("F1:", f)
