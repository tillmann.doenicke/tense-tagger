for filename in $1/*.txt; do
    filename=$(basename $filename)
    python split_sentences.py $1/$filename > temp/temp2.txt
    java -Xmx2G -cp anna-3.61.jar is2.lemmatizer.Lemmatizer -model ger-tagger+lemmatizer+morphology+graph-based-3.6/lemma-ger-3.6.model -test temp/temp2.txt -out temp/temp3.txt
    java -Xmx2G -cp anna-3.61.jar is2.tag.Tagger  -model ger-tagger+lemmatizer+morphology+graph-based-3.6/tag-ger-3.6.model -test temp/temp3.txt -out temp/temp4.txt
    java -Xmx2G -cp anna-3.61.jar is2.mtag.Tagger  -model ger-tagger+lemmatizer+morphology+graph-based-3.6/morphology-ger-3.6.model -test temp/temp4.txt -out temp/temp5.txt
    java -Xmx3G -classpath anna-3.61.jar is2.parser.Parser -model ger-tagger+lemmatizer+morphology+graph-based-3.6/parser-ger-3.6.model -test temp/temp5.txt -out temp/$filename
    sh annoTMV.sh -i temp/$filename -l de -h no
done