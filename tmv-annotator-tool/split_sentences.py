import spacy
import sys

if len(sys.argv) > 1:
    filename = sys.argv[1]
    with open(filename, "r") as f:
        text = f.read()
        sp = spacy.load("de_core_news_lg", disable=["ner"])
        sents = sp(text).sents
        for i, sent in enumerate(sents):
            for k, token in enumerate(sent):
                if not token.is_space:
                    print("\t".join([str(k+1), token.text, "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]))
                else:
                    print("\t".join([str(k+1), " ", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_", "_"]))
            print()