import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from settings import *

# pipeline components
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer, tiger_clausizer, ud_clausizer
from pipeline.components.pipeline_helper import lemma_fixer
from pipeline.components.sentencizer import nltk_sentencizer, spacy_sentencizer
from pipeline.components.tense_tagger import rb_tense_tagger
