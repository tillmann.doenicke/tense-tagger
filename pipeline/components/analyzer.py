import copy
import spacy
from demorphy import Analyzer
from spacy.lang.de.tag_map import TAG_MAP
from spacy.tokens import Token

from .pipeline_helper import add_extension


class Morph():
    """A class that can store morphological analysis information of a token.
        To access an attribute of a token, e.g. tense, use:
            token._.morph.tense_
            token._.morph.tense
        The first expression returns all possible tenses of the token in a set (the set can be empty).
        The second expression resturns the tense of the token if it is existing and unambiguous; otherwise it returns None.

    """    
    def __init__(self, spacy_features={}, demorphy_features={}):
        """__init__ method of the class `Morph`.

        Args:
            spacy_features (dict of str:(set of str)): Morphological features of a spacy token.
                The features of a token can be accessed through TAG_MAP[token.tag_] (but have be converted to the desired format).
            demorphy_features (dict of str:(set of str)): Morphological features of a demorphy analysis.
                The features of a token can be accessed through Analyzer().analyze(token.text) (but have be converted to the desired format).
        
        """
        # (all) features from spacy and possible values:
        self._spacy_feats = [
            'AdpType', # 'circ', 'prep', 'post'
            'Aspect', # 'perf'
            'ConjType', # 'comp'
            'Foreign', # 'yes'
            'Hyph', # 'yes'
            'Mood', # 'ind', 'imp'
            'NumType', # 'card'
            'PartType', # 'inf', 'res', 'vbp'
            'Polarity', # 'neg'
            'Poss', # 'yes'
            'PronType', # 'dem', 'ind|neg|tot', 'prs', 'rel', 'int', 'art'
            'PunctType', # 'peri', 'comm', 'brck'
            'Reflex', # 'yes'
            'VerbForm', # 'fin', 'inf', 'part'
            'VerbType' # 'mod'
        ]

        # (some) features from demorphy and possible values:
        self._demorphy_feats = [
            "CASE", # "nom", "acc", "dat", "gen"
            "PERSON", # "1per", "2per", "3per"
            "NUMERUS", # "sing", plu
            "GENDER", # "masc", "fem", "neut", "noGender"
            "TENSE", # "pres", "ppres", "past", "ppast"
            "MODE", # "imp", "ind", "subj"
            "INFLECTION", # "inf", "zu"
            "DEGREE", # "pos", "comp", "sup"
            "STARKE", # "strong", "weak"
            "ADDITIONAL_ATTRIBUTES" # "<mod>", "<aux>", "<adv>", "<pred>", "<ans>", "<attr>", "<adj>", "<cmp>", "<coord>", "<def>", "<indef>", "<noinfl>", "<neg>", "personal", "<prfl>", "<rec>", "<pro>", "<refl>", "<subord>", "<subst>"
        ]

        # add both feature sets as attributes
        # (attribute names are lowercased and camelcases are changed to underscores):
        for feat in self._spacy_feats + self._demorphy_feats:
            feat = feat.replace("Type", "_Type").replace("Form", "_Form").lower()
            setattr(self, feat, None)
            setattr(self, feat + "_", set())
        
        # set the attributes' values:
        self._integrate_values(spacy_features)
        self._integrate_values(demorphy_features)

    
    def _integrate_values(self, dd):
        """Method to fill feature-values pairs into the object's attributes.

        Args:
            dd (dict of str:(set of str)): feature-values pairs
        
        """
        for key in dd.keys():
            val = dd[key]
            try:
                key = key.replace("Type", "_Type").replace("Form", "_Form").lower()
                if hasattr(self, key):
                    if len(val) == 1:
                        setattr(self, key, list(val)[0])
                    setattr(self, key + "_", val)
            except:
                pass


    def as_dict(self):
        """Convert the object's attributes to a dictionary.
            Only keep those attributes which have at least one value.
        
        Returns:
            dict of str:(set of str): Dictionary with morphological features and the corresponding values.
        
        """
        as_dict = vars(self)
        as_dict = {key : as_dict[key] for key in as_dict if key.endswith("_") and len(as_dict[key]) > 0}
        return as_dict
    
    
    def __str__(self):
        """__str__ method of the class `Morph`.

        Returns:
            str: String representation of the object's attributes.
                Only shows used attributes.
        
        """
        return str(self.as_dict())


def demorphy_analyzer(doc):
    """Spacy pipeline component.
        Uses the demorphy tool to analyse the tokens of a document.
        If loaded before the parser, the analyses of each word are independent of the other words.
        If loaded after the parser, the analyses are limited with respect to case-number-gender agreement.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    # Initialise analyser and new token attribute, then fill attribute for each token:
    analyzer = Analyzer(char_subs_allowed=True).analyze
    add_extension(Token, "morph")
    features_analyses = []
    for token in doc:
        
        # retrieve the morphological features from the spacy object
        features = TAG_MAP[token.tag_]
        features = {key : set([features[key]]) for key in features if type(key) != int}

        # analyse with demorphy
        analyses = analyzer(token.text)
        analyses = [ass._fields for ass in analyses]

        # since demorphy is case sensitive, add the lowercased forms
        # on the beginning of a sentence. If the document is not yet split into sentences,
        # add the lowercased forms anyways:
        if token.is_sent_start or not doc.is_sentenced:
            analyses2 = analyzer(token.lower_)
            analyses2 = [ass._fields for ass in analyses2]
            analyses += analyses2
        
        # filter the different analyses by STTS tag
        # (the "STTS_TAG" field of an analysis is a string or a list of strings
        # and the strings sometimes only contain the prefix of an STTS tag,
        # whereas token.tag_ is always a correct STTS tag;
        # all of these cases are handled in the following):
        filtered = []
        for ass in analyses:
            stts_tags = ass["STTS_TAG"]
            if type(stts_tags) == str:
                stts_tags = [stts_tags]
            stts_tags = [tag for tag in stts_tags if token.tag_.startswith(tag)]
            if len(stts_tags) > 0:
                ass["STTS_TAG"] = stts_tags
                filtered.append(ass)
        
        # Store the spacy and demorphy features in a list first;
        # in a second iteration, the features will be assigned to the tokens.
        features_analyses.append((features, filtered))
    
    # assign the features to the tokens; 
    # if the document is already parsed, filter the features according to CNG congruence.
    i = 0
    if doc.is_parsed and doc.is_sentenced:
        for sent in doc.sents:
            features_analyses = filter_cng(sent, features_analyses)
            for token in sent:
                features, analyses = features_analyses[i]
                analysis = unify_analyses(analyses)
                token._.morph = Morph(features, analysis)
                i += 1
    else:
        for token in doc:
            features, analyses = features_analyses[i]
            analysis = unify_analyses(analyses)
            token._.morph = Morph(features, analysis)
            i += 1

    return doc


def unify_analyses(analyses):
    """Combine several analyses of a demorphy analyser into one.

    Args:
        analyses (list of (dict of str:obj)): The analyses of a demorphy analyser in dictionary format.
            The values are mostly strings but sometimes lists of strings or even boolean.
    
    Returns:
        dict of str:(set of str): A single dictionary combining all the information from the different analyses.
            Each key existing in at least one of the analysis is contained in the result dict;
            the values of the results dict are sets containing all the value from the input analyses.
    """
    unify = {}
    if len(analyses) > 0:
        for ass in analyses:
            keys = ass.keys()
            for key in keys:
                if key not in unify:
                    unify[key] = set()
                val = values_set(ass[key])
                unify[key] = unify[key].union(val)
    return unify


def values_set(val):
    """Converts a demorphy value into a set of values.

    Args:
        val (obj): A demorphy value is commonly a string but sometimes a lists of strings or something else.
    
    Returns:
        set of obj: A set of the individual values.
    
    """
    if type(val) == list:
        val = set(val)
    elif type(val) == str:
        val = set(val.split(","))
    else:
        val = set([val])
    return val


def filter_cng(sent, f_a):
    """Filters the demorphy analyses of the words in a sentence with respect to their case-number-gender congruence.

    Args:
        sent (`Span`): The sentence.
        f_a (list of (dict of str:str,list of (dict of str:obj))): The `feature_analyses` list for the whole document.
            For each token there is a tuple containing the spacy features and the demorphy analyses.
    
    Returns:
        list of (dict of str:str,list of (dict of str:obj)): A filtered version of `feature_analyses` 
            where the demeoprhy analyses of the tokens in the sentence have been filtered.
    
    """
    for x in range(2):
        # (since the following rules influence each othger, we iterate as many times as there are rules;
        # a better solution would be to iterate until the number of analyses is stable, i.e. did not change
        # in an iteration, but this is not implemented yet.)
        
        # congruence within NP
        for token in sent:
            if token.tag_ in ["NN", "NE"]:
                js = get_nmod_is(token)
                if len(js) > 0:
                    lst = intersect_cng([f_a[token.i]] + [f_a[j] for j in js], ["CASE", "NUMERUS", "GENDER"])
                    f_a[token.i] = lst[0]
                    for k, j in enumerate(js):
                        f_a[j] = lst[1+k]

        # congruence of finite verb and nominal subject
        for token in sent:
            if token.tag_.endswith("FIN"):
                j = get_subj_i(token)
                if j > -1:
                    lst = intersect_cng([f_a[token.i], f_a[j]], ["NUMERUS", "PERSON"], {"CASE" : "nom"})
                    f_a[token.i] = lst[0]
                    f_a[j] = lst[1]
    
        # space for more ...

    return f_a


def intersect_cng(f_a_list, feats, fixed_feats={}, empty_intersections=False):
    """Intersect a list of analyses according to some features.

    Args:
        f_a_list (list of (dict of str:str,list of (dict of str:obj))): The `feature_analyses` tuples for the words whose analyses should be intersected.
        feats (list of str): List of demorphy features to intersect on.
        fixed_feats (dict of str:str): A dictionary that maps a demorphy feature to a value. Only analyses with that feature-value combination are kept (does not affect analyses without that feature).
        empty_intersections (boolean): If False then the analyses will only be reduced if at least one analysis is left. If True, the analyses will be reduced even if no analysis is left.
    
    Returns:
        list of (dict of str:str,list of (dict of str:obj)): Updated analyses.
            Reduced example "auf dem Stuhl":
                Input: [
                    [
                        {'LEMMA': 'auf', 'CASE': 'acc', 'STTS_TAG': ['APPR']}, 
                        {'LEMMA': 'auf', 'CASE': 'dat', 'STTS_TAG': ['APPR']}
                    ], 
                    [
                        {'LEMMA': 'die', 'CASE': 'dat', 'NUMERUS': 'sing', 'GENDER': 'masc', 'STTS_TAG': ['ART']}, 
                        {'LEMMA': 'die', 'CASE': 'dat', 'NUMERUS': 'sing', 'GENDER': 'neut', 'STTS_TAG': ['ART']}
                    ], 
                    [
                        {'LEMMA': 'Stuhl', 'CASE': 'acc', 'NUMERUS': 'sing', 'GENDER': 'masc', 'STTS_TAG': ['NN']}, 
                        {'LEMMA': 'Stuhl', 'CASE': 'dat', 'NUMERUS': 'sing', 'GENDER': 'masc', 'STTS_TAG': ['NN']}, 
                        {'LEMMA': 'Stuhl', 'CASE': 'nom', 'NUMERUS': 'sing', 'GENDER': 'masc', 'STTS_TAG': ['NN']}
                    ]   ]
                Output: [
                    [
                        {'LEMMA': 'auf', 'CASE': 'dat', 'STTS_TAG': ['APPR']}
                    ], 
                    [
                        {'LEMMA': 'die', 'CASE': 'dat', 'NUMERUS': 'sing', 'GENDER': 'masc', 'STTS_TAG': ['ART']}
                    ], 
                    [
                        {'LEMMA': 'Stuhl', 'CASE': 'dat', 'NUMERUS': 'sing', 'GENDER': 'masc', 'STTS_TAG': ['NN']}
                    ]   ]
    
    """
    f_s = [f_a[0] for f_a in f_a_list]
    a_s = [f_a[1] for f_a in f_a_list]
    
    # if "PERSON" is a feature of interest, we have to add "3per" as default to words without a person value
    # (by doing so, nouns etc. will get a person value);
    person = []
    if "PERSON" in feats:
        for i, ass in enumerate(a_s):
            person.append([])
            for j, a in enumerate(ass):
                person[i].append([])
                if not "PERSON" in a:
                    if "MODE" in a and a["MODE"] == "imp":
                        # if the word is an imperative then "2per" instead of "3per" is added:
                        a_s[i][j]["PERSON"] = "2per"
                    else:
                        a_s[i][j]["PERSON"] = "3per"
                    person[i][j] = True
                else:
                    person[i][j] = False
    
    a_s_old = copy.deepcopy(a_s)
    
    # repeat reducing analyses until stable
    all_congruent = False
    while not all_congruent:
        all_congruent = True

        # for each feature, collect those values which appear in all words
        # (except those words which don't have that feature)
        inters = {}
        for feat in feats:
            vals = []
            for ass in a_s:
                ass_vals = set()
                for a in ass:
                    if feat in a:
                        val = values_set(a[feat])
                        ass_vals.update(val)
                if len(ass_vals) > 0:
                    vals.append(ass_vals)
            if len(vals) > 0:
                inters[feat] = set.intersection(*vals)
            else:
                inters[feat] = set()
        
        # add those features that should have a specific value
        for feat in fixed_feats:
            inters[feat] = set([fixed_feats[feat]])
        
        # only keep those analyses with common features
        for i, ass in enumerate(a_s):
            ass_new = []
            for a in ass:
                congruent = True
                for feat in inters:
                    if feat in a:
                        val = values_set(a[feat])
                        is_val = set.intersection(inters[feat], val)
                        if len(is_val) == 0:
                            congruent = False
                            break
                if congruent:
                    ass_new.append(a)
                else:
                    all_congruent = False
            a_s[i] = ass_new
            """
            if len(ass_new) > 0 or empty_intersections:
                a_s[i] = ass_new
            elif len(ass_new) == 0:
                all_congruent = True
            """
    
    # overwrite the original analyses with the reduced analyses
    # if the list of analyses for every word is not empty or if empty analyses are allowed;
    # otherwise use the original list of analyses
    all_not_empty = True
    for i, ass in enumerate(a_s):
        if not (len(ass) > 0 or len(a_s_old[i]) == 0):
            all_not_empty = False
            break
    if not (all_not_empty or empty_intersections):
        a_s = a_s_old
    
    # remove the added person values
    if "PERSON" in feats:
        for i, ass in enumerate(a_s):
            for j, a in enumerate(ass):
                if person[i][j]:
                    del a_s[i][j]["PERSON"]
    
    return [(f_s[i], a_s[i]) for i in range(len(f_s))]


def get_subj_i(token):
    """Return the nominal subject of a token.
        Works for UD and TIGER dependencies.
    
    Args:
        sent (`Span`): The sentence.
    
    Returns:
        int: The index (`.i`) of the nominal subject.
            Returns -1 if there is no nominal subject.
    
    """
    while True:
        for child in token.children:
            if child.dep_.split(":")[0] in ["nsubj", "sb"]:
                return child.i
        if token.i == token.head.i: # ROOT
            break
        token = token.head
    return -1


def get_nmod_is(token):
    """Return the modifiers of a noun.
        Modifiers are determiners, adjectives and adpositions.
        Works for UD and TIGER dependencies.
    
    Args:
        token (`Token`): The noun.
    
    Returns:
        list of int: The indices (`.i`) of the modifiers.
    
    """
    nmods = []
    for child in token.children:
        dep = child.dep_.split(":")[0]
        if dep in ["det", "amod", "nk"] and child.pos_ in ["DET", "ADJ"]:
            nmods.append(child.i)
        elif dep == "case" and child.pos_ == "ADP": # in UD, the adpositions are dependents of nouns
            nmods.append(child.i)
    if token.head.pos_ == "ADP" and token.dep_ == "nk": # in TIGER, the adpositions are heads of nouns
        nmods.append(token.head.i)
    return nmods