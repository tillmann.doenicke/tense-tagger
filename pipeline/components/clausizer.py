from spacy.tokens import Doc, Span, Token

from .pipeline_helper import add_extension


def _conjunct_is_always_clause(token, dep_labels):
    return True

def _conjunct_is_never_clause(token, dep_labels):
    return False

def _conjunct_is_clause_if_its_head_is_clause(token, dep_labels):
    return token.head.dep_ in dep_labels and token.head.dep_ not in CONJUNCTS

def _conjunct_is_clause_if_its_head_is_root(token, dep_labels):
    return token.sent.root == token.head

# conjunct relations ("conj" in UD; "cd" in TIGER)
CONJUNCTS = ["conj", "cd"]

# conjunct handling rule (choose one of the implemented functions or implement your own)
CONJUNCTION_RULE = _conjunct_is_clause_if_its_head_is_clause


def _xcomp_is_always_clause(token, dep_labels):
    return True

def _xcomp_is_never_clause(token, dep_labels):
    return False

def _xcomp_is_clause_if_it_is_complex(token, dep_labels):
    tokens = token.subtree
    verbs = 0
    words = 0
    for token in tokens:
        if token.is_punct or token.is_space:
            continue
        if token.pos_ in ["VERB", "AUX"]:
            verbs += 1
        else:
            dep = token.dep_.split(":")[0]
            if dep == "compound" or (dep == "mark" and token.pos_ == "PART"):
                continue
            words += 1
    return verbs > 0 and words > 0

# xcomp relations ("xcomp" in UD)
XCOMPS = ["xcomp"]

# open-clausal-complement handling rule (choose one of the implemented functions or implement your own)
XCOMP_RULE = _xcomp_is_clause_if_it_is_complex


def dependency_clausizer(doc):
    """Spacy pipeline component.
        Splits sentences of a document into clauses.
        Detects the dependency label scheme (UD or TIGER) and calls the corresponding clausizer.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    ud_labels = set([
        "acl", "advcl", "advmod", "amod", "appos", "aux", "case", "cc", "ccomp", "clf", 
        "compound", "conj", "cop", "csubj", "dep", "det", "discourse", "dislocated", "expl", "fixed", 
        "flat", "goeswith", "iobj", "list", "mark", "nmod", "nsubj", "nummod", "obj", "obl", 
        "orphan", "parataxis", "punct", "reparandum", "vocative", "xcomp"
    ])
    tiger_labels = set([
        "ac", "adc", "ag", "ams", "app", "avc", "cc", "cd", "cj", "cm", 
        "cp", "cvc", "da", "dep", "dm", "ep", "ju", "mnr", "mo", "ng", 
        "nk", "nmc", "oa", "oc", "og", "op", "par", "pd", "pg", "ph", 
        "pm", "pnc", "punct", "rc", "re", "rs", "sb", "sbp", "svp", "uc", 
        "vo"
    ])
    inters = ud_labels.intersection(tiger_labels)
    ud_labels.difference_update(inters)
    tiger_labels.difference_update(inters)
    for token in doc:
        dep = token.dep_.split(":")[0]
        if dep in ud_labels:
            return ud_clausizer(doc)
        elif dep in tiger_labels:
            return tiger_clausizer(doc)
    return ud_clausizer(doc)


def ud_clausizer(doc):
    """Spacy pipeline component.
        Splits sentences of a document into clauses, buy cutting specific Universal Dependencies relations.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    dep_labels = [
        "ROOT",      # matrix sentence
        "acl",       # adjectival clause
        "advcl",     # adverbial clause
        "ccomp",     # clausal complement
        "csubj",     # clausal subject
        "csubjpass", # + clausal subject (passive)
        "discourse", # interjections
        "intj",      # + interjections
        "parataxis", # parataxis
        "pcomp",     # + prepositional complement
        "relcl",     # + relative clause
        "vocative",  # vocative
        "xcomp",     # open clausal complement
        "conj",      # conjunct
        "list"       # list (kind-of conjunct)
    ]
    # relations with a `+` are relations from the Stanford Dependencies,
    # which are slightly different from UD relations.
    return _clausizer(doc, dep_labels)


def tiger_clausizer(doc):
    """Spacy pipeline component.
        Splits sentences of a document into clauses, buy cutting specific TIGER dependency relations.

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    dep_labels = [
        # This list is incomplete since we focused on the `ud_clausizer`.
        # Update this list to get proper results!
        "ROOT", # matrix sentence
        "oc",   # clausal object
        "rc",   # relative clause
        "cd"    # coordinating conjunction
    ]
    return _clausizer(doc, dep_labels)


def _clausizer(doc, dep_labels):
    """Splits sentences of a document into clauses, buy cutting specific dependency relations.

    Args:
        doc (`Doc`): A spacy document object.
        dep_labels (list of str): Dependency relations that qualify as root nodes of clauses.
    
    Returns:
        `Doc`: A spacy document object.

    """
    add_extension(Doc, "clauses")
    add_extension(Span, "clauses")
    add_extension(Span, "tokens")
    add_extension(Span, "prec_punct")
    add_extension(Span, "succ_punct")
    add_extension(Token, "clause")
    all_clauses = []
    for sent in doc.sents:
        clauses = find_clauses(list(sent), dep_labels)
        clauses = strip_punct_and_convert(doc, clauses)
        clauses = [clause for clause in clauses if len(clause) > 0] # empty spans cause errors when accessing
        clauses = sorted(clauses, key=lambda clause: (list(clause)+list(clause._.succ_punct))[0].i)
        sent._.clauses = clauses
        all_clauses.extend(clauses)
    doc._.clauses = all_clauses
    for clause in all_clauses:
        for token in clause._.tokens:
            token._.clause = clause
    return doc


def strip_punct_and_convert(doc, clauses, include_ws=False):
    """Convert clauses to spacy spans.
        Preceding and Succeeding punctuation is not included in a span but saved in according attributes of the span.

    Args:
        doc (`Doc`): The parent document.
        clauses (list of (list of Token)): List of clauses. Clauses are lists of tokens.
        include_ws (boolean): True iff whitespace tokens should be included (in this case they are treated as punctuation).
            If so, all tokens of the document are included in a clause. Otherwise, whitespace tokens don't belong to any clause.
    
    Returns:
        list of `Span`: List of clauses.
    
    """
    for i, clause in enumerate(clauses):
        succ_punct = []
        for j in range(len(clause)):
            token = clause[-j-1]
            if token.is_punct or token.is_space:
                succ_punct.append(token)
            else:
                break
        prec_punct = []
        if len(succ_punct) < len(clause):
            for j in range(len(clause)):
                token = clause[j]
                if token.is_punct or token.is_space:
                    prec_punct.append(token)
                else:
                    break
            start = clause[len(prec_punct)].i
            end = clause[-len(succ_punct)-1].i
            clauses[i] = doc[start:end+1]
            clauses[i]._.tokens = [token for token in clauses[i] if token in clause]
        else:
            clauses[i] = Doc(doc.vocab, [])[0:0]
            clauses[i]._.tokens = []
        if not include_ws:
            prec_punct = [token for token in prec_punct if not token.is_space]
            succ_punct = [token for token in succ_punct if not token.is_space]
        clauses[i]._.prec_punct = prec_punct
        clauses[i]._.succ_punct = list(reversed(succ_punct))
    return clauses


def find_clauses(sent, dep_labels, root=-1):
    """Find all clauses in a sentence.

    Args:
        sent (`Span`): A sentence in spacy format.
        dep_labels (list of str): Dependency relations that qualify as root nodes of clauses.
        root (int): The root of the current subclause. -1 if called on the matrix sentence.
    
    Returns:
        list of (list of Token): List of clauses.
    
    """
    clauses = set()
    root_tokens = [token for token in sent if is_clause_root(token, dep_labels) and not token.i == root]
    while len(root_tokens) > 0:
        token = root_tokens.pop()
        subtree = list(token.subtree)
        sub_clauses = find_clauses(subtree, dep_labels, token.i)
        for clause in sub_clauses:
            for tok in clause:
                subtree.remove(tok)
            clauses.add(tuple(clause))
        clauses.add(tuple(subtree))
    return list(clauses)


def is_clause_root(token, dep_labels):
    """Check whether a token is the root of a clause.
        Handles conjunctions as specified in CONJUNCTION_RULE.

    Args:
        token (`Token`): The token.
        dep_labels (list of str): Dependency relations that qualify as root nodes of clauses.
    
    Returns:
        boolean: True iff the token is the root of a clause.
    
    """
    if token.dep_ in dep_labels:
        if token.dep_ in CONJUNCTS:
            return CONJUNCTION_RULE(token, dep_labels)
        elif token.dep_ in XCOMPS:
            return XCOMP_RULE(token, dep_labels)
        return True
    return False
