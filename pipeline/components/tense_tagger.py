import itertools
import json
import os
from spacy.tokens import Span

from .analyzer import Morph
from .clausizer import CONJUNCTS
from .pipeline_helper import add_extension

from .. import FLEXION_PATH
from .. import WIKTIONARY_TOOLS_PATH


class Form():
    """A class that can store surface-syntactical analysis information of a clause.
        To access an attribute of a clause, e.g. tense, use:
            clause._.form.tense

    """    
    def __init__(self):
        """__init__ method of the class `Form`.

        """
        self._feats = [
            "tense", # 'pres', 'past', 'fut
            "aspect", # 'imperf', 'perf'
            "mode", # 'imp', 'ind', 'subj:pres', 'subj:past'
            "voice", # 'active', 'pass', 'pass:dynamic', 'pass:static'
            "verb_form", # 'fin', 'inf', 'part'
            "main", # (main verb `Token`)
            "modals", # (list of modal verbs `Token`)
            "verbs" # (list of verbs `Token`)
        ]
        
        # add features as attributes
        for feat in self._feats:
            setattr(self, feat, None)
        
        # change default value for verb lists from None to empty list
        self.main = None
        self.modals = []
        self.verbs = []

    
    def as_dict(self):
        """Convert the object's attributes to a dictionary.
        
        Returns:
            dict of str:(set of str): Dictionary with syntactical features and the corresponding values.
        
        """
        as_dict = vars(self)
        as_dict = {key : as_dict[key] for key in self._feats}
        return as_dict


    def __str__(self):
        """__str__ method of the class `Form`.

        Returns:
            str: String representation of the object's attributes.
                Only shows used attributes.
        
        """
        return str(self.as_dict())


def get_aux_verbs(only_one_form=False):
    """Returns all possible auxiliary verbs for German verbs.

    Args:
        only_one_form (boolean): True if only the more common auxiliary should be returned; False otherwise.
    
    Returns:
        dict of str:(list of str): Each key is a lemma of a verbs; each value is a list with the possible auxiliaries ("haben" and/or "sein").
    
    """
    with open(os.path.join(WIKTIONARY_TOOLS_PATH, "verb_tense.json"), "r") as f:
        aux_verbs = json.load(f)
        if only_one_form:
            aux_verbs = {verb : aux_verbs[verb][0] for verb in aux_verbs if len(aux_verbs[verb]) > 0}
        return aux_verbs


def get_verb_forms(only_one_form=False):
    """Returns all possible German verb forms (tense + aspect) and analyses.

    Args:
        only_one_form (boolean): True if only the most plausible analysis should be returned; False otherwise.
    
    Returns:
        dict of (str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str,str):(list of (str,str,str,str)): A dictionary mapping a form to its analyses.
            The form is expressed as a str vector:
                0: contains an infinite form of the verb? "inf": yes, "" : no
                1: contains an inflected present form of the verb? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                2: contains an inflected past form of the verb? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                3: contains a participle form of the verb? "pres" : present participle, "past" : past participle, "" : none
                4: contains an infinite form of 'haben'? "inf": yes, "" : no
                5: contains an inflected present form of 'haben'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                6: contains an inflected past form of 'haben'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                7: contains a participle form of 'haben'? "pres" : present participle, "past" : past participle, "" : none
                8: contains an infinite form of 'sein'? "inf": yes, "" : no
                9: contains an inflected present form of 'sein'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                10: contains an inflected past form of 'sein'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                11: contains a participle form of 'sein'? "pres" : present participle, "past" : past participle, "" : none
                12: contains an infinite form of 'werden'? "inf": yes, "" : no
                13: contains an inflected present form of 'werden'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                14: contains an inflected past form of 'werden'? "imp" : imperative, "ind" : indicative, "subj" : subjunctive, "" : none
                15: contains a participle form of 'werden'? "pres" : present participle, "past" : past participle, "" : none
                16: the auxiliary verb: "haben" or "sein"
            An analysis is a tuple:
                0: tense : "pres", "past" or "fut"
                1: aspect : "imperf" or "perf"
                2: voice : "active", "pass", "pass:dynamic" or "pass:static"
                3: mode : "imp", "ind", "subj:pres", "subj:past", "inf" or "part"
                
    """
    forms = {}
    with open(os.path.join(FLEXION_PATH, "flexion.csv")) as f:
        lines = [line.strip().split(",") for line in f.readlines()]
        for i, row in enumerate(lines):
            if i == 1:
                keys = row[1:5]
            if i > 1:
                aux = row[0]
                analysis = tuple(row[1:5])
                form = tuple(row[5:-1]) + (aux,)
                example = row[-1]
                try:
                    forms[form].add(analysis)
                except KeyError:
                    forms[form] = set([analysis])
        for form in sorted(forms):
            if len(forms[form]) == 1 or not only_one_form:
                forms[form] = list(forms[form])
            else:
                # sometimes the form is ambiguous between an active and a passive form;
                # in this case we select the active form because the competing passive forms are hardly used:
                forms[form] = [sorted(filter(lambda x: x[2] == "active", list(forms[form])))[0]]
    return forms


# verb tables
VERB_FORMS = None
AUX_VERBS = None


def rb_tense_tagger(doc, voice_preference="active"):
    """Spacy pipeline component.
        Rule-based tense tagger.

    Args:
        doc (`Doc`): A spacy document object.
        voice_preference (str): If several analyses are possible, they can be filtered according to voice.
            "active": always prefer active voice
            "passive" or "pass": always prefer passive voice
            "auto": use the dependency parser to determine the voice of a clause
    
    Returns:
        `Doc`: A spacy document object.

    """
    global VERB_FORMS
    global AUX_VERBS
    if VERB_FORMS is None or AUX_VERBS is None:
        # load verb tables
        VERB_FORMS = get_verb_forms()
        AUX_VERBS = get_aux_verbs()

    if voice_preference == "passive":
        voice_preference = "pass"
    
    add_extension(Span, "form")
    for clause in doc._.clauses:
        analysis = Form()

        # get infinite and finite verbs
        infinite_verbs = [token for token in clause._.tokens if token.pos_ in ["AUX", "VERB"] and token._.morph.verb_form != "fin"]
        finite_verbs = [token for token in clause._.tokens if token.pos_ in ["AUX", "VERB"] and token._.morph.verb_form == "fin"]
        infinite_verbs = sorted(infinite_verbs, key=lambda token: token.i)
        finite_verbs = sorted(finite_verbs, key=lambda token: token.i)
        
        # a clause should contain only one finite verb
        # if there happen to be more than one, this is usually caused by non-split conjuncts;
        # in this case, we just select the last one
        if len(finite_verbs) > 1:
            finite_verbs = [finite_verbs[-1]]

        verbs = infinite_verbs + finite_verbs

        # if the clause is a conjunct, it is necessary to copy the missing verbs from the matrix clause 
        # (the preceding conjunct)
        if clause.root.dep_ in CONJUNCTS:
            for prec_clause in clause.root.head.sent._.clauses:
                if clause.root.head in prec_clause._.tokens:
                    """
                    print(clause.root.head.sent)
                    print([(token if token.pos_ not in ["AUX", "VERB"] else (token, token.pos_, token._.morph.verb_form)) for token in prec_clause._.tokens])
                    print([(token if token.pos_ not in ["AUX", "VERB"] else (token, token.pos_, token._.morph.verb_form)) for token in clause._.tokens])
                    print(verbs, prec_clause._.form.verbs)
                    """
                    if prec_clause._.form is not None:
                        prec_verbs = prec_clause._.form.verbs # these are the verbs from the matrix clause
                        if len(verbs) == 0:
                            # if there are no verbs in the conjunct, we copy all verbs from the matrix clause
                            verbs.extend(prec_verbs)
                        elif len(prec_verbs) > 0:
                            # otherwise, we copy the missing succeeding and preceding verbs
                            s_verbs = [verb for verb in prec_verbs[:-1] if verb.i > verbs[-1].i]
                            p_verbs = [verb for verb in prec_verbs[:-1] if verb.i < verbs[0].i]
                            # the finite verb of the matrix clause has to go to the end (as for the conjunct)
                            if prec_verbs[-1].i > verbs[-1].i or prec_verbs[-1]._.morph.verb_form == "fin":
                                s_verbs.append(prec_verbs[-1])
                            else:
                                p_verbs.append(prec_verbs[-1])
                            if verbs[-1]._.morph.verb_form != "fin":
                                # 1) the conjunct has no finite verb:
                                #    we might have to copy syntactically high verbs from the matrix clause,
                                #      "[dass Hans Maria gesehen [und gerufen] hat]"
                                #      -> gesehen hat; gerufen hat
                                for i in range(0, len(s_verbs)+1):
                                    if i == len(s_verbs) or verbs[-1]._.morph.verb_form == s_verbs[-(i+1)]._.morph.verb_form:
                                        verbs.extend(s_verbs[len(s_verbs)-i:])
                                        break
                            if True:
                                # 2) we might have to copy syntactically low verbs from the matrix clause,
                                #      "[dass Hans Maria gesehen hat] [und hatte]"
                                #      -> gesehen hat; gesehen hatte
                                for i in range(0, len(p_verbs)+1):
                                    if i == len(p_verbs) or verbs[0]._.morph.verb_form == p_verbs[i]._.morph.verb_form:
                                        verbs = p_verbs[:i] + verbs
                                        break
                    """
                    print(verbs)
                    input()
                    """
                    break

        if len(verbs) > 0:

            # group verbs into categories
            category = []
            for token in verbs:
                if token._.morph.verb_type == "mod":
                    category.append("mod")
                elif token.pos_ == "VERB":
                    category.append("main")
                elif token.lemma_ in ["haben", "sein", "werden"]:
                    category.append(token.lemma_)
                else:
                    category.append("")
            
            # select the main verb
            if category.count("main") == 0:
                category[0] = "main"
            while category.count("main") > 1:
                category[category.index("main")] = ""
            
            # ignore all verbs before the main verb (only verbs between main verb and finite verb are important)
            category = [(c if i >= category.index("main") else "") for i, c in enumerate(category)]
            
            # morphological features of the verbs
            morphs = [[verb._.morph] for verb in verbs]

            # convert pseudo-infinitive (of modal verbs and in AcI constructions) to past participle
            # (only occurs when finite verb is form of "haben")
            if category[-1] == "haben" and len(verbs) > 1 and morphs[-2][0].verb_form == "inf":
                morphs[-2].append(Morph({"VerbForm" : {'part'}, "Aspect" : {'perf'}}, {"TENSE" : {'ppast'}}))

            # copy the features of modal verbs to the non-modal verbs (modal verbs are ignored later on)
            for i in reversed(range(len(verbs))):
                if category[i] == "mod":
                    morphs[i-1] = morphs[i]
                elif category[i] == "main":
                    break
            
            analyses = set()
            
            # shift the main verb rightwards until an analysis is found
            mk = category.index("main")
            for k in range(mk, len(verbs)):
                if k > mk:
                    category[k-1] = ""
                category[k] = "main"
            
                # extract the form vector
                all_possible_forms = itertools.product(*morphs)
                all_form_vectors = []
                for form in all_possible_forms:
                    cats = ["main", "haben", "sein", "werden"]
                    form_vectors = {c : form_vector() for c in cats}
                    for i, morph in enumerate(form):
                        if category[i] in cats:
                            form_vectors[category[i]] = form_vector(morph, form_vectors[category[i]])
                    form = form_vectors["main"] + form_vectors["haben"] + form_vectors["sein"] + form_vectors["werden"]
                    all_form_vectors.append(form)
                
                # get possible analyses (for possible auxiliaries)
                aux = ["haben"] # always add "haben" (because of potential auxilary change by modal verbs; and because "haben" forms are unambiguous anyways)
                if verbs[category.index("main")].lemma_ not in AUX_VERBS or "sein" in AUX_VERBS[verbs[category.index("main")].lemma_]:
                    aux.append("sein")
                for form in all_form_vectors:
                    for a in aux:
                        try:
                            analyses.update(VERB_FORMS[tuple(form) + (a,)])
                        except KeyError:
                            pass
                
                if len(analyses) > 0:
                    break
            
            # filter analyses by voice
            if voice_preference == "auto":
                voice = "active"
                for token in clause._.tokens:
                    if token.dep_ in ["nsubj:pass", "csubj:pass", "sbp"]: # subject-passive dependencies in UD and TIGER
                        voice = "pass"
                        break
            else:
                voice = voice_preference
            filtered = list(filter(lambda x: x[2].startswith(voice), analyses))
            if len(filtered) > 0:
                analyses = filtered
            
            """
            print(
                [verb for i, verb in enumerate(verbs) if category[i] == "main"],
                [verb for i, verb in enumerate(verbs) if category[i] == "haben"], 
                [verb for i, verb in enumerate(verbs) if category[i] == "sein"],
                [verb for i, verb in enumerate(verbs) if category[i] == "werden"], 
                [verb for i, verb in enumerate(verbs) if category[i] == "mod"],
                [verb for i, verb in enumerate(verbs) if category[i] == ""],
                analyses
            )
            """

            # select an analysis
            form = Form()
            if len(analyses) > 0:
                analysis = sorted(analyses).pop(0)
                form.tense = analysis[0]
                form.aspect = analysis[1]
                form.voice = analysis[2]
                if analysis[3] in ["inf", "part"]:
                    form.verb_form = analysis[3]
                else:
                    form.verb_form = "fin"
                    form.mode = analysis[3]
            form.main = verbs[category.index("main")]
            form.modals = [verbs[i] for i, c in enumerate(category) if c == "mod"]
            form.verbs = [verbs[i] for i, c in enumerate(category) if c != ""]
            analysis = form
            
        clause._.form = analysis
    return doc


def form_vector(verb=None, form=None):
    """Calculate a vector representation for a verb and update the corresponding form vector if given.
        `form_vector()` returns an empty form vector.
        `form_vector(verb)` returns the form vector for `verb`.
        `form_vector(verb, form)` returns the `form` after updating it with `verb`.

    Args:
        verb (`Morph`): The morphological analysis if a verb.
        form (list of str): An existing form vector.
    
    Returns:
        list of str: The new form vector.
    
    """
    if form is None:
        form = ["" for i in range(4)]
    if verb is not None:
        if verb.verb_form == "inf": # spacy
            form[0] = "inf"
        elif verb.verb_form == "fin": # spacy
            if verb.mood == "imp": # spacy
                form[1] = "imp"
            elif "pres" in verb.tense_: # demorphy
                if "ind" in verb.mode_: # demorphy
                    form[1] = "ind"
                elif "subj" in verb.mode_: # demorphy
                    form[1] = "subj"
                elif "imp" in verb.mode_: # demorphy
                    form[1] = "imp"
            elif "past" in verb.tense_: # demorphy
                if "ind" in verb.mode_: # demorphy
                    form[2] = "ind"
                elif "subj" in verb.mode_: # demorphy
                    form[2] = "subj"
            elif len(verb.tense_) == 0: # demorphy
                if verb.mode == "imp": # demorphy
                    form[1] = "imp"
                elif verb.mood == "ind": # spacy
                    form[1] = "ind"
                elif verb.mode == "ind": # demorphy
                    form[1] = "ind"
                elif verb.mode == "subj": # demorphy
                    form[1] = "subj"
        elif verb.verb_form == "part": # spacy
            if "ppast" in verb.tense_: # demorphy
                form[3] = "past"
            elif "ppres" in verb.tense_: # demorphy
                form[3] = "pres"
            elif len(verb.tense_) == 0: # demorphy
                form[3] = "past"
        # (spacy features always have one value; 
        # demorphy features can have zero or several values)
    return form


def get_highest_verb(clause):
    """Return the syntactically highest verb in a clause (or any other span).

    Args:
        clause (`Span`): A clause.
    
    Returns:
        `Token`: A token.
    
    """
    roots = [clause.root]
    root = None
    while len(roots) > 0:
        root = roots.pop(0)
        if root.pos_ in ["VERB", "AUX"]:
            break
        roots.extend(list(root.children))
        root = None
    return root