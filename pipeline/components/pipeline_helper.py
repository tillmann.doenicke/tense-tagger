"""This file contains useful methods for developing the SpaCy pipeline.
"""

def add_extension(obj, name, default=None):
    """Add a custom extension to a spacy class.

    Args:
        obj (obj): The spacy class (`Doc`, `Span` or `Token`).
        name (str): The name of the attribute.
        default (obj): The default value of the extension.
    
    """
    try:
        obj.set_extension(name, default=default)
    except ValueError:
        pass


def lemma_fixer(doc):
    """Spacy pipeline component.
        Fix wrong lemmas from the built-in spacy lemmatizer.
        Spacy basically lemmatises using this list:
            https://raw.githubusercontent.com/explosion/spacy-lookups-data/master/spacy_lookups_data/data/de_lemma_lookup.json

    Args:
        doc (`Doc`): A spacy document object.
    
    Returns:
        `Doc`: A spacy document object.

    """
    # List of words to be re-lemmatised
    # (the idea is to add frequent words here to keep the list short;
    # not every uncommon word spacy has trouble with):
    exceptions = {
        # lowercased word : { POS tag : new lemma }
        "muß" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "mußtest" : dict.fromkeys(["AUX", "VERB"], "müssen"),
        "sein" : dict.fromkeys(["AUX", "VERB"], "sein"),
        "soll" : dict.fromkeys(["AUX", "VERB"], "sollen"),
        "wolle" : dict.fromkeys(["AUX", "VERB"], "wollen")
    }
    for token in doc:
        if token.lower_ in exceptions and token.pos_ in exceptions[token.lower_]:
            token.lemma_ = exceptions[token.lower_][token.pos_]
    return doc


class Debugger():
    """A simple class for debugging.
    
    """
    def __init__(self, debug=False):
        self.DEBUG = debug

    def print(self, *values):
        if self.DEBUG:
            print(*values)

    def input(self):
        if self.DEBUG:
            input()
