import os
import spacy
import sys
import tqdm
from lxml import etree
from spacy_conll import ConllFormatter
from statsmodels.stats.inter_rater import fleiss_kappa


def read_plain_texts(path_sourcedocs):
    """Read plain texts.

    Args:
        path_sourcedocs (str): Path to the directory containing the plain text documents.
    
    Returns:
        dict of str:str: Dictionary mapping the text name (= filename without extension and trailing underscore) to the text.
    
    """
    texts = {}
    for filename in os.listdir(path_sourcedocs):
        if filename.endswith(".txt"):
            name = filename[:-4]
            if name.endswith("_"):
                name = name[:-1]
            texts[name] = open(os.path.join(path_sourcedocs, filename), "r").read()
    return texts


def read_annotations(path_annotations, evaluation, texts):
    """Read annotations from CATMA files.

    Args:
        path_annotations (str): Path to the directory containing the CATMA files.
        evaluation (str): "don-quijote" or "heureclea"
        texts (dict of str:str): Output of `read_plain_texts`.

    Returns:
        dict of str:(dict of int:(dict of str:(set of str))): Dictionary mapping the text name to an annotation dictionary.
            The annotation dictionary maps a character position to a dictionary which maps an annotator to a set.
            Each set contains the annotations of the annotator.
    
    """
    tagged_texts = {text : {} for text in texts}
    for filename in os.listdir(path_annotations):
        if filename.endswith(".xml"):
            if evaluation == "don-quijote":
                annotator = filename[:-4]
                annotation = "tenses"
                name = list(texts.keys())[0]
            elif evaluation == "heureclea":
                parts = filename[:-4].split(" ")
                annotator = parts[-1]
                annotation = parts[-2]
                name = "_".join(parts[:-2])
            if annotation == "tenses":
                tagset = {}
                segments = []
                tagged = {}
                for event, elem in etree.iterparse(os.path.join(path_annotations, filename), events=("end",), recover=True):
                    tag = elem.tag.split("}")[-1]
                    if tag == "fsDecl" and "type" in elem.attrib:
                        for descr in elem:
                            if descr.tag.split("}")[-1] == "fsDescr":
                                tagset[elem.attrib["type"]] = descr.text
                                break
                    elif tag == "ab" and "type" in elem.attrib and elem.attrib["type"] == "catma":
                        for segment in elem:
                            seg_tag = segment.tag.split("}")[-1]
                            if seg_tag == "ptr":
                                ana = None
                                target = segment.attrib["target"]
                            elif seg_tag == "seg":
                                ana = [a[1:] for a in segment.attrib["ana"].split(" ")]
                                target = list(segment.iter()).pop().attrib["target"]
                            target = tuple([int(c) for c in target.split("#")[-1].split("=")[-1].split(",")])
                            segments.append({"chars" : target, "tags" : ana})
                    elif tag == "fs" and "type" in elem.attrib:
                        for a in elem.attrib:
                            if a.split("}")[-1] == "id":
                                tagged[elem.attrib[a]] = elem.attrib["type"]
                                break
                n = 0
                for i in range(len(segments)):
                    if segments[i]["tags"] is not None:
                        segments[i]["tags"] = set([tagset[tagged[a]] for a in segments[i]["tags"]])
                    else:
                        segments[i]["tags"] = set()
                    start, end = segments[i]["chars"]
                    if evaluation == "heureclea": # in heureclea, every newline shifts the character position
                        start -= n
                        end -= n
                    text = texts[name][start:end]
                    n += text.count("\n")
                    segments[i]["text"] = text
                    for c in range(start, end):
                        try:
                            tagged_texts[name][c][annotator] = segments[i]["tags"]
                        except KeyError:
                            tagged_texts[name][c] = {annotator : segments[i]["tags"]}
    return tagged_texts


def read_dq_gold(path_sourcedocs):
    """Read gold annotations of "Don Quijote".

    Args:
        path_sourcedocs (str): Path to the CATMA file.
    
    Returns:
        dict of int:obj: Dictionary mapping a character position to None if untagged or a dictionary with annotations.
            If not None, the keys of the dictionary are "Tense", "Mood", "Voice", "Modality". The values are of type string or None.
        list of (dict of str:obj): List of all annotation segments. A segment is represented by a dictionary:
            "chars": start index and end index of the segment
            "tags": dictionary with annotations
    
    """
    tagged_text = {}
    segments = []
    tagged = {}
    for event, elem in etree.iterparse(os.path.join(path_sourcedocs, "gold.xml"), events=("end",), recover=True):
        tag = elem.tag.split("}")[-1]
        if tag == "ab" and "type" in elem.attrib and elem.attrib["type"] == "catma":
            for segment in elem:
                seg_tag = segment.tag.split("}")[-1]
                if seg_tag == "ptr":
                    ana = None
                    target = segment.attrib["target"]
                elif seg_tag == "seg":
                    ana = [a[1:] for a in segment.attrib["ana"].split(" ")].pop()
                    target = list(segment.iter()).pop().attrib["target"]
                target = tuple([int(c) for c in target.split("#")[-1].split("=")[-1].split(",")])
                segments.append({"chars" : target, "tags" : ana})
        elif tag == "fs" and "type" in elem.attrib:
            for a in elem.attrib:
                if a.split("}")[-1] == "id":
                    tmv = {}
                    for child in elem:
                        if child.tag.split("}")[-1] == "f":
                            for b in child.attrib:
                                if b.split("}")[-1] == "name" and child.attrib[b] in ["Tense", "Mood", "Voice", "Modality"]:
                                    tmv[child.attrib[b]] = None
                                    for c in child:
                                        if c.tag.split("}")[-1] == "string":
                                            tmv[child.attrib[b]] = c.text
                    tagged[elem.attrib[a]] = tmv
                    break
    for i in range(len(segments)):
        if segments[i]["tags"] is not None:
            segments[i]["tags"] = tagged[segments[i]["tags"]]
        else:
            segments[i]["tags"] = None
        start, end = segments[i]["chars"]
        for c in range(start, end):
            tagged_text[c] = segments[i]["tags"]
    return tagged_text, segments


def read_tmv_annotator_results(tmv_path):
    """Read tmv-annotator results.

    Args:
        tmv_path (str): Path to the directory containing the output files of the tmv-annotator.
    
    Returns:
        dict of str:(dict of int:(dict of int:str)): Dictionary mapping the text name to a token dictionary.
            The word dictionary maps a sentence number to a dictionary which maps a token number to a token.
        dict of str:(dict of int:(dict of int:obj)): Dictionary with the same structure as the first output.
            The innermost values are not tokens but either tuples or sets of tuples.
                A tuple is used for tagged verbs: (tense, mood, voice).
                A set of such tuples us used for all other words.
    
    """
    tmv_texts = {}
    tmv_tagged_texts = {}
    for filename in os.listdir(tmv_path):
        if filename.endswith(".verbs"):
            name = filename[:-10]
            if name.endswith("_"):
                name = name[:-1]
            tmv_texts[name] = {}
            tmv_tagged_texts[name] = {}
            with open(os.path.join(tmv_path, filename), "r") as f:
                lines = f.readlines()
                for line in lines:
                    line = line.strip().split("\t")
                    sent = int(line[0])
                    tokens = [int(x) for x in line[1].split(",")]
                    words = line[2].split(" ")
                    tmv = tuple(line[5:8])
                    if sent not in tmv_texts[name]:
                        tmv_texts[name][sent] = {}
                        tmv_tagged_texts[name][sent] = {}
                    for i, token in enumerate(tokens):
                        tmv_texts[name][sent][token] = words[i]
                        tmv_tagged_texts[name][sent][token] = tmv
                    if len(line) >= 11:
                        clause = line[10][:-4].split(" ")
                        for i, word in enumerate(clause):
                            index = tokens[0]-[w.lower() for w in clause].index(words[0])+i
                            if index not in tmv_texts[name][sent] or type(tmv_tagged_texts[name][sent][index]) != tuple:
                                tmv_texts[name][sent][index] = word
                                try:
                                    tmv_tagged_texts[name][sent][index].add(tmv)
                                except KeyError:
                                    tmv_tagged_texts[name][sent][index] = set([tmv])
    return tmv_texts, tmv_tagged_texts


# import spacy components
from settings import PARSING_PATH
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.tense_tagger import rb_tense_tagger
from pipeline.components.pipeline_helper import lemma_fixer

def build_spacy_pipeline():
    """Build SpaCy pipeline.

    Returns:
        `spacy': A spacy pipeline.
    
    """
    sp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
    sp.add_pipe(lemma_fixer, name="lemma_fixer", after="tagger")
    sp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
    sp.add_pipe(demorphy_analyzer, name="analyzer")
    sp.add_pipe(dependency_clausizer, name="clausizer")
    sp.add_pipe(rb_tense_tagger, name="tense_tagger")
    sp.add_pipe(ConllFormatter(sp, include_headers=True))
    return sp


def get_annotation_result(token, tagged_texts, name):
    """Return tense annotation for token.

    Args:
        token (`token`): The token.
        tagged_texts (dict of str:(dict of int:(dict of str:(set of str)))): Output of `read_annotations`.
        name (str): Key of the current text.
    
    Returns:
        str: tense
        str: mood (None)
        str: voice (None)
        str: modality (None)
        str: finiteness (None)
    
    """
    gold = "untagged"
    try:
        annotations = tagged_texts[name][token.idx]
        annotations = set([tuple(annotations[annotator]) for annotator in annotations])
        if len(annotations) == 1 and len(list(annotations)[0]) == 1:
            gold = list(annotations)[0][0]
        elif len(annotations) > 0 and sum([len(a) for a in annotations]) > 0:
            gold = "undefined"
    except KeyError:
        pass
    return gold, None, None, None, None


def get_dq_annotation_result(token, tagged_text, merge_to_tmv):
    """Return "Don Quijote" gold annotation for token.

    Args:
        token (`token`): The token.
        tagged_text (dict of int:obj): Output of `read_dq_gold`.
        merge_to_tmv (boolean): Iff true, the tags of our tagger are merged to the coarser-grained tags of the tmv-annotator.
    
    Returns:
        str: tense
        str: mood (if existing)
        str: voice (if existing)
        str: modality (if existing)
        str: finiteness (if existing)
    
    """
    gold = {"Tense" : "untagged", "Mood" : "untagged", "Voice" : "untagged", "Modality" : "untagged"}
    gold["Finiteness"] = "untagged"
    if token.idx in tagged_text and tagged_text[token.idx] is not None:
        gold = tagged_text[token.idx]
        gold = {p : (gold[p] if gold[p] is not None else "undefined") for p in gold}
        gold["Finiteness"] = "undefined"
        if gold["Mood"] in ["(infinitive)", "(participle)"]:
            gold["Finiteness"] = "infinite"
            gold["Mood"] = "undefined"
        elif gold["Mood"] != "undefined":
            gold["Finiteness"] = "finite"
            if gold["Mood"] == "present_subjunctive":
                gold["Mood"] = "subjunctiveI"
            elif gold["Mood"] == "past_subjunctive":
                gold["Mood"] = "subjunctiveII"
            elif merge_to_tmv and gold["Mood"] == "imperative":
                gold["Mood"] = "indicative"
        if merge_to_tmv:
            if gold["Voice"].endswith("passive"):
                gold["Voice"] = "passive"
            else:
                gold["Voice"] = "active"
    return gold["Tense"], gold["Mood"], gold["Voice"], gold["Modality"], gold["Finiteness"]


def get_tmv_annotator_result(token, j, i, tmv_texts, tmv_tagged_texts, name, evaluation):
    """Return TMV result for token.

    Args:
        token (`token`): The token.
        j (int): Sentence index.
        i (int): Token index.
        tmv_texts (dict of str:(dict of int:(dict of int:str))): Output of `read_tmv_annotator_results`.
        tmv_tagged_texts (dict of str:(dict of int:(dict of int:obj))): Output of `read_tmv_annotator_results`.
        name (str): Key of the current text.
        evaluation (str): gold tags ("don-quijote" or "heureclea")
    
    Returns:
        str: tense
        str: mood (if existing)
        str: voice (if existing)
        str: modality (if existing)
        str: finiteness (if existing)
    
    """
    offset = 0
    if j+1 not in tmv_texts[name]:
        pass
    elif i+1 not in tmv_texts[name][j+1] or token.text != tmv_texts[name][j+1][i+1]:
        for d in range(5):
            if i+1+d in tmv_texts[name][j+1] and token.text == tmv_texts[name][j+1][i+1+d]:
                offset = d
                break
            elif i+1-d in tmv_texts[name][j+1] and token.text == tmv_texts[name][j+1][i+1-d]:
                offset = -d
                break
    tmv_pred = "untagged"
    tmv_pred_mood, tmv_pred_voice, tmv_pred_finite = "untagged", "untagged", "untagged"
    try:
        if token.text == tmv_texts[name][j+1][i+1+offset]:
            tmv_ = tmv_tagged_texts[name][j+1][i+1+offset]
            if type(tmv_) == tuple:
                tmv_ = set([tmv_])
            if len(tmv_) == 1:
                tmv_ = list(tmv_).pop()
                tense, mood, voice = tmv_
                if tense in ["present", "perfect", "pluperfect"]:
                    tmv_pred = tense
                elif tense == "imperfect":
                    tmv_pred = "preterite"
                elif tense == "futureI":
                    tmv_pred = "future"
                elif tense == "futureII":
                    tmv_pred = "future_perfect"
                    if evaluation == "heureclea":
                        tmv_pred = "future"
                if mood == "indicative":
                    tmv_pred_mood = "indicative"
                elif mood.startswith("konjunktiv"):
                    tmv_pred_mood = mood.replace("konjunktiv", "subjunctive")
                if voice == "active":
                    tmv_pred_voice = "active"
                elif voice == "passive":
                    tmv_pred_voice = "passive"
                tmv_pred_finite = "finite"
                if (tense, mood, voice) == ("-", "-", "-"):
                    tmv_pred_finite = "infinite"
            elif len(tmv_) > 1:
                tmv_pred = "undefined"
                tmv_pred_mood, tmv_pred_voice, tmv_pred_finite = "undefined", "undefined", "undefined"
    except:
        pass
    return tmv_pred, tmv_pred_mood, tmv_pred_voice, None, tmv_pred_finite


def get_tense_tagger_result(token, doc, evaluation, merge_to_tmv):
    """Return tense tagger result for token.

    Args:
        token (`token`): The token.
        doc (`doc`): The spacy document.
        evaluation (str): gold tags ("don-quijote" or "heureclea")
        merge_to_tmv (boolean): Iff true, the tags of our tagger are merged to the coarser-grained tags of the tmv-annotator.
    
    Returns:
        str: tense
        str: mood (if existing)
        str: voice (if existing)
        str: modality (if existing)
        str: finiteness (if existing)
    
    """
    tense, aspect = None, None
    mood, voice, modality, finiteness = None, None, None, None
    pred = "untagged"
    pred_mood, pred_voice, pred_modality, pred_finiteness = "untagged", "untagged", "untagged", "untagged"
    for clause in doc._.clauses:
        if token in clause._.tokens:
            tense, aspect = clause._.form.tense, clause._.form.aspect
            mood, voice, modality, finiteness = clause._.form.mode, clause._.form.voice, clause._.form.modals, clause._.form.verb_form
            pred = "undefined"
            pred_mood, pred_voice, pred_modality, pred_finiteness = "undefined", "undefined", "undefined", "undefined"
            break
    if tense is not None and aspect is not None:
        if tense == "pres" and aspect == "imperf":
            pred = "present"
        elif tense == "pres" and aspect == "perf":
            pred = "perfect"
        elif tense == "past" and aspect == "imperf":
            pred = "preterite"
        elif tense == "past" and aspect == "perf":
            pred = "pluperfect"
        elif tense == "fut" and aspect == "imperf":
            pred = "future"
        elif tense == "fut" and aspect == "perf":
            pred = "future_perfect"
            if evaluation == "heureclea":
                pred = "future"
        if mood == "ind":
            pred_mood = "indicative"
        elif mood == "imp":
            pred_mood = "imperative"
            if merge_to_tmv:
                pred_mood = "indicative"
        elif mood == "subj:pres":
            pred_mood = "subjunctiveI"
        elif mood == "subj:past":
            pred_mood = "subjunctiveII"
        elif mood is None:
            pred_mood = "undefined"
        if voice == "active":
            pred_voice = "active"
        elif merge_to_tmv:
            pred_voice = "passive"
        elif voice == "pass:dynamic":
            pred_voice = "dynamic_passive"
        elif voice == "pass:static":
            pred_voice = "static_passive"
        else:
            pred_voice = "passive"
        if len(modality) != 1:
            pred_modality = "undefined"
        else:
            pred_modality = modality[0].lemma_
        if finiteness == "fin":
            pred_finiteness = "finite"
        else:
            pred_finiteness = "infinite"
    return pred, pred_mood, pred_voice, pred_modality, pred_finiteness


def get_annotation_vector(token, tagged_texts, name, all_tags, annotators):
    """Return annotation vector for token.
        (A list of these vectors is the input for the calculation of Fleiss' Kappa.)

    Args:
        token (`token`): The token.
        tagged_texts (dict of str:(dict of int:(dict of str:(set of str)))): Output of `read_annotations`.
        name (str): Key of the current text.
        all_tags (list of str): List of tags (without "undefined"/"untagged").
        annotators (list of str): List of annotators.
    
    Returns:
        list of int: vector.
    
    """
    anno_vector = [0] * len(all_tags)
    anno_vector.append(len(annotators))
    annotations = None
    if token.idx in tagged_texts[name]:
        annotations = tagged_texts[name][token.idx]
        for annotator in annotators:
            if annotator in annotations and len(annotations[annotator]) == 1:
                tag = list(annotations[annotator])[0]
                if tag in all_tags:
                    anno_vector[all_tags.index(tag)] += 1
                    anno_vector[-1] -= 1
    return anno_vector


def get_tags_and_annotators(tagged_texts):
    """Return the tags and the annotators of an annotation.

    Args:
        tagged_texts (dict of str:(dict of int:(dict of str:(set of str)))): Output of `read_annotations`.
    
    Returns:
        list of str: List of tags (without "undefined"/"untagged").
        list of str: List of annotators.
    
    """
    all_tags = set.union(*[set.union(*[set.union(*[tagged_texts[name][c][annotator] for annotator in tagged_texts[name][c]]) for c in tagged_texts[name]]) for name in tagged_texts])
    all_tags.difference_update(["undefined", "untagged", "past"]) # "past" occurs a few times in heureclea for some reason but is no valid tense
    try:
        annotators = list(list(tagged_texts.values())[0].values())[0].keys()
    except:
        annotators = []
    return sorted(list(all_tags)), sorted(list(annotators))


def print_anno_confusion_matrix(anno_matrix12, all_tags):
    """Print confusion matrix of the annotators.
        Only works if there are exactly two annotators.
    
    Args:
        anno_matrix12 (list of (list of int,list of int)): List of tuples of one-hot annotation vectors for both annotators.

    """
    crosstbl = [[0 for t1 in range(len(all_tags)+1)] for t2 in range(len(all_tags)+1)]
    for anno_vector1, anno_vector2 in anno_matrix12:
        i = anno_vector1.index(1)
        j = anno_vector2.index(1)
        crosstbl[i][j] += 1
    print("Confusion matrix")
    print(all_tags + ["unndef./untag."])
    for row in crosstbl:
        print(row)


def evaluate_tense(texts, tagged_texts, tmv_texts, tmv_tagged_texts, sp, evaluation, pos_tags=None, log_lines=False):
    """Tense evaluation.

    Args:
        texts (dict of str:str): Output of `read_plain_texts`.
        tagged_texts (dict of str:(dict of int:(dict of str:(set of str)))): Output of `read_annotations`.
        tmv_texts (dict of str:(dict of int:(dict of int:str))): Output of `read_tmv_annotator_results`.
        tmv_tagged_texts (dict of str:(dict of int:(dict of int:obj))): Output of `read_tmv_annotator_results`.
        sp (`spacy`): Spacy pipeline.
        evaluation (str): gold tags ("don-quijote" or "heureclea")
        pos_tags (list): List of UPOS tags. Only tokens with this tags are used for accuracy calculation.
            If None, all tokens are used.
            If ["VERB"], only verbs are used.
        log_lines (boolean): True iff tab-separated columns should be logged:
            token, UPOS, gold tag, tmv-annotator tag, predicted tag.
    
    Returns:
        float: Fleiss' Kappa of the annotations.
        float: Accuracy of tmv-annotator.
        float: Accuracy of our predictions.

    """
    a = 0     # all
    c = 0     # correct (tense tagger)
    c_tmv = 0 # correct (tmv-annotator)
    anno_matrices = []
    anno_matrices12 = []
    all_tags, annotators = get_tags_and_annotators(tagged_texts)
    sout = sys.stdout
    sys.stdout = open(os.path.join("evaluate_tense_tagger_logs", "tense_" + evaluation + ".tsv"),'wt')
    for name in texts:
        text = texts[name]
        doc = sp(text)
        anno_matrix = []
        anno_matrix12 = []
        end_of_annotation = len(doc)-1
        for j, sent in enumerate(doc.sents):
            for i, token in enumerate(sent):
                if token.pos_ == "SPACE":
                    continue
                
                # annotations for Fleiss' Kappa
                anno_vector = get_annotation_vector(token, tagged_texts, name, all_tags, annotators)
                anno_vector1 = get_annotation_vector(token, tagged_texts, name, all_tags, [annotators[0]])
                anno_vector2 = get_annotation_vector(token, tagged_texts, name, all_tags, [annotators[1]])

                # annotations (heureclea or don-quijote)
                gold, _, _, _, _ = get_annotation_result(token, tagged_texts, name)
                if gold != "untagged":
                    end_of_annotation = token.i
                
                # tmv-annototor results
                tmv_pred, _, _, _, _ = get_tmv_annotator_result(token, j, i, tmv_texts, tmv_tagged_texts, name, evaluation)
                
                # tense tagger results
                pred, _, _, _, _ = get_tense_tagger_result(token, doc, evaluation, True)
                
                if log_lines:
                    print("\t".join([token.text, token.pos_, gold, tmv_pred, pred]))
                if pos_tags is None or token.pos_ in pos_tags:
                    anno_matrix.append(anno_vector)
                    anno_matrix12.append((anno_vector1, anno_vector2))
                    if gold not in ["untagged", "undefined"]:
                        a += 1
                        if gold == tmv_pred:
                            c_tmv += 1
                        if gold == pred:
                            c += 1
        if evaluation == "heureclea":
            anno_matrix = anno_matrix[:end_of_annotation+1]
            anno_matrix12 = anno_matrix12[:end_of_annotation+1]
        anno_matrices.extend(anno_matrix)
        anno_matrices12.extend(anno_matrix12)
    sys.stdout = sout
    #print_anno_confusion_matrix(anno_matrices12, all_tags)
    return fleiss_kappa(anno_matrices), 1.0*c_tmv/a, 1.0*c/a


def evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, cat, merge_to_tmv=True, only_finites=False, pos_tags=None, log_lines=False):
    """TMV evaluation.

    Args:
        texts (dict of str:str): Output of `read_plain_texts`.
        tagged_texts (dict of str:(dict of int:(dict of str:(set of str)))): Output of `read_annotations`.
        tagged_text (dict of int:obj): Output of `read_dq_gold`.
        tmv_texts (dict of str:(dict of int:(dict of int:str))): Output of `read_tmv_annotator_results`.
        tmv_tagged_texts (dict of str:(dict of int:(dict of int:obj))): Output of `read_tmv_annotator_results`.
        sp (`spacy`): Spacy pipeline.
        cat (int): tense (0), mood (1), voice (2), modality (3), finiteness (4)
        merge_to_tmv (boolean): Iff true, the tags of our tagger are merged to the coarser-grained tags of the tmv-annotator.
        only_finites (boolean): True iff only finite forms should be used for accuracy calculation.
        pos_tags (list): List of UPOS tags. Only tokens with this tags are used for accuracy calculation.
            If None, all tokens are used.
            If ["VERB"], only verbs are used.
        log_lines (boolean): True iff tab-separated columns should be logged:
            token, UPOS, gold tag, tmv-annotator tag, predicted tag.
    
    Returns:
        float: Accuracy of tmv-annotator.
        float: Accuracy of our predictions.

    """
    a = 0     # all
    c = 0     # correct (tense tagger)
    c_tmv = 0 # correct (tmv-annotator)
    evaluation = "don-quijote"
    name = "don-quijote-vorrede"
    text = texts[name]
    doc = sp(text)
    sout = sys.stdout
    sys.stdout = open(os.path.join("evaluate_tense_tagger_logs", "dq_" + str(cat) + ("_unmerged" if not merge_to_tmv else "") + ".tsv"),'wt')
    for j, sent in enumerate(doc.sents):
        for i, token in enumerate(sent):
            if token.pos_ == "SPACE":
                continue
            
            # gold annotations
            gold = get_dq_annotation_result(token, tagged_text, merge_to_tmv)

            # tmv-annototor results
            tmv_pred = get_tmv_annotator_result(token, j, i, tmv_texts, tmv_tagged_texts, name, evaluation)

            # tense tagger results
            pred = get_tense_tagger_result(token, doc, evaluation, merge_to_tmv)

            if log_lines and token.pos_ != "SPACE":
                print("\t".join([token.text, token.pos_, str(gold[4]), str(gold[cat]), str(tmv_pred[cat]), str(pred[cat])]))
            if pos_tags is None or token.pos_ in pos_tags:
                if gold[cat] not in ["untagged", "undefined"] and (gold[4] == "finite" or not only_finites):
                    a += 1
                    if gold[cat] == tmv_pred[cat]:
                        c_tmv += 1
                    if gold[cat] == pred[cat]:
                        c += 1
    sys.stdout = sout
    return 1.0*c_tmv/a, 1.0*c/a
            

def text_with_boundaries(text, boundaries):
    """Insert clause boundaries into a plain text.

    Args:
        text (str): The plain text.
        boundaries (list of (int,int)): Sorted list of boundaries (end of a clause, start of next clause).
    
    Returns:
        str: The plain text with square brackets around the clauses.
    
    """
    for a in reversed(boundaries):
        a1, a2 = a
        text = text[:a1] + " ] " + text[a1:a2] + " [ " + text[a2:]
    return "[ " + text + " ]"


def evaluate_clauses(texts, segments, sp):
    """Clausizer evaluation.

    Args:
        texts (dict of str:str): Output of `read_plain_texts`.
        segments (list of (dict of str:obj)): List of all annotation segments.
        sp (`spacy`): Spacy pipeline.
    
    Returns:
        float: Precision of the clause boundary detection.
        float: Recall of the clause boundary detection.
        float: F-Score of the clause boundary detection.
    
    """
    evaluation = "don-quijote"
    name = "don-quijote-vorrede"
    text = texts[name]
    doc = sp(text)
    subclauses = []
    for clause in doc._.clauses:
        subclause = []
        for i, token in enumerate(clause._.tokens):
            if len(subclause) == 0 or token.i == subclause[-1].i+1:
                subclause.append(token)
            else:
                subclauses.append(subclause)
                subclause = [token]
        if len(subclause) > 0:
            subclauses.append(subclause)
    subclauses = sorted(subclauses, key=lambda subclause: subclause[0].i)
    pred_boundaries = list()
    for i, subclause in enumerate(subclauses[1:]):
        s1, e1 = subclauses[i][0].idx, subclauses[i][-1].idx+len(subclauses[i][-1].text)
        s2, e2 = subclauses[i+1][0].idx, subclauses[i+1][-1].idx+len(subclauses[i+1][-1].text)
        if e1 != s2:
            pred_boundaries.append((e1,s2))
    gold_boundaries = list()
    segments = [segment for segment in segments if segment["tags"] is not None]
    for i, segment in enumerate(segments[1:]):
        s1, e1 = segments[i]["chars"]
        s2, e2 = segments[i+1]["chars"]
        if e1 != s2:
            gold_boundaries.append((e1,s2))
    #print(text_with_boundaries(text, gold_boundaries))
    #print(text_with_boundaries(text, pred_boundaries))
    gold = set(gold_boundaries)
    pred = set(pred_boundaries)
    precision = 1.0*len(gold.intersection(pred))/len(pred)
    recall = 1.0*len(gold.intersection(pred))/len(gold)
    fscore = 2.0*precision*recall/(precision+recall)
    print("Gold:", len(gold))
    print("Pred:", len(pred))
    return precision, recall, fscore


if __name__ == "__main__":
    sp = build_spacy_pipeline()

    evaluation = "heureclea"
    sourcedocs = "sourcedocuments"
    annotations = "time-annotations-compared-public"
    path_sourcedocs = os.path.join("heureclea", sourcedocs)
    path_annotations = os.path.join("heureclea", annotations)
    tmv_path = os.path.join("tmv-annotator-tool", "output")

    texts = read_plain_texts(path_sourcedocs)
    tagged_texts = read_annotations(path_annotations, evaluation, texts)
    tmv_texts, tmv_tagged_texts = read_tmv_annotator_results(tmv_path)

    print("heureCLEA (tokens)")
    kappa, tmv_acc, acc = evaluate_tense(texts, tagged_texts, tmv_texts, tmv_tagged_texts, sp, evaluation, None, True)
    print("Ramm :", tmv_acc)
    print("New  :", acc)
    
    print("heureCLEA (verbs)")
    kappa, tmv_acc, acc = evaluate_tense(texts, tagged_texts, tmv_texts, tmv_tagged_texts, sp, evaluation, ["VERB"], True)
    print("Ramm :", tmv_acc)
    print("New  :", acc)
    
    evaluation = "don-quijote"
    path_sourcedocs = os.path.join("don-quijote-vorrede")
    path_annotations = os.path.join("don-quijote-vorrede", "annotationcollections")
    tmv_path = os.path.join("tmv-annotator-tool")
    
    texts = read_plain_texts(path_sourcedocs)
    tagged_texts = read_annotations(path_annotations, evaluation, texts)
    tmv_texts, tmv_tagged_texts = read_tmv_annotator_results(tmv_path)
    tagged_text, segments = read_dq_gold(path_sourcedocs)
    
    print("Don Quijote (tokens)")
    kappa, tmv_acc, acc = evaluate_tense(texts, tagged_texts, tmv_texts, tmv_tagged_texts, sp, evaluation, None, True)
    print("Kappa:", kappa)
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 0, True, False, None, False)
    print("Ramm :", tmv_acc)
    print("New  :", acc)
    
    print("Don Quijote (verbs)")
    kappa, tmv_acc, acc = evaluate_tense(texts, tagged_texts, tmv_texts, tmv_tagged_texts, sp, evaluation, ["VERB"], True)
    print("Kappa:", kappa)
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 0, True, False, ["VERB"], False)
    print("Ramm :", tmv_acc)
    print("New  :", acc)
    
    print("Finiteness (verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 4, True, False, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Tense (finite verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 0, True, True, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Mood (finite verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 1, True, True, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Voice (finite verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 2, True, True, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Modality (finite verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 3, True, True, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)

    print("Tense (verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 0, False, False, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Mood (verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 1, False, False, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Voice (verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 2, False, False, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)

    print("Modality (verbs)")
    tmv_acc, acc = evaluate_tmv(texts, tagged_texts, tagged_text, tmv_texts, tmv_tagged_texts, sp, 3, False, False, ["VERB"], True)
    print("Ramm:", tmv_acc)
    print("New :", acc)
    
    print("Clauses")
    p, r, f = evaluate_clauses(texts, segments, sp)
    print("Pr:", p)
    print("Re:", r)
    print("F1:", f)