# Clause-Level Tense, Mood, Voice and Modality Tagging for German

<b style="color:red;">Notice:</b> Newer versions of this tagger can be found [here](https://gitlab.gwdg.de/mona/pipy-public).

## Table of contents

- [Set-up](#set-up)
  - [SpaCy](#spacy)
  - [DEMorphy](#demorphy)
- [Example](#example)
- [SpaCy models with UD relations](#spacy-models-with-UD-relations)
- [Analyzer, clausizer and tense(-mood-voice-...) tagger](#analyzer,-clausizer-and-tense-mood-voice-...-tagger)
- [TLT 2020](#tlt-2020)
- [License/Citation](#licensecitation)

## Set-up

Clone the repository, set-up a virtual environment and install the requirements:

```sh
git clone https://gitlab.gwdg.de/tillmann.doenicke/tense-tagger.git
cd tense-tagger
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

### SpaCy

Download spaCy models:

```sh
python -m spacy download de_core_news_lg
python -m spacy download en_core_web_lg
```

### DEMorphy

```sh
git clone https://github.com/DuyguA/DEMorphy
cd DEMorphy
```

Manually download `words.dg` from [https://github.com/DuyguA/DEMorphy/blob/master/demorphy/data/words.dg](https://github.com/DuyguA/DEMorphy/blob/master/demorphy/data/words.dg) and replace `demorphy/data/words.dg`.

Install DEMorphy and copy the file to your site-packages (you probably have to change the path to your site-packages):

```sh
python setup.py install
cp demorphy/data/words.dg ../env/lib/python3.7/site-packages/demorphy-1.0-py3.7.egg/demorphy/data/
cd ..
```

## Example

Test the pipeline by running:

```sh
python example.py
```

This should print the output:

```
[Der, Mann, begann]
[begann]
fin past imperf ind active []

[der, die, ulkige, Kuh, gesehen, haben, musste]
[gesehen, haben, musste]
fin past perf ind active [musste]

[laut, zu, lachen]
[lachen]
inf pres imperf None active []
```

## SpaCy models with UD relations

We trained new parsers on the German Universal Dependencies corpora and incorporated it into the existing spaCy model `de_core_news_lg`. The new spaCy models are saved in `resources/parsing`:

| model name | trained on                                                 | tested on          | acc. |
| ---------- | ---------------------------------------------------------- | ------------------ | ---- |
| de_ud_sm   | PUD test, LIT test                                         | GSD test, HDT test | 0.65 |
| de_ud_md   | PUD test, LIT test, GSD dev, HDT dev                       | GSD test, HDT test | 0.83 |
| de_ud_lg   | PUD test, LIT test, GSD dev, HDT dev, GSD train, HDT train | GSD test, HDT test | 0.85 |

The models can be loaded as usual in spaCy:

```python
import spacy

sp = spacy.load("resources/parsing/de_ud_lg")
```

**Note:** During training, we somehow lost the integrated sentencizer of the original model. It is therefore necessary to add a custom sentencizer to the spaCy pipeline before the parser:

```python
from pipeline.components.sentencizer import spacy_sentencizer

sp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
```

## Analyzer, clausizer and tense(-mood-voice-...) tagger

Our custom spaCy components are located in `pipeline/components` and can be easily added to the SpaCy pipeline:

```python
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.tense_tagger import rb_tense_tagger

sp.add_pipe(demorphy_analyzer, name="analyzer")
sp.add_pipe(dependency_clausizer, name="clausizer")
sp.add_pipe(rb_tense_tagger, name="tense_tagger")
```

These components add the following attributes to spaCy objects:

| Attribute           | Type          | Description                        |
| ------------------- | ------------- | ---------------------------------- |
| **clausizer**       |               |                                    |
| doc._.clauses       | list of Span  | All clauses of the document.       |
| sent._.clauses      | list of Span  | All clauses of the sentence.       |
| clause._.prec_punct | list of Token | Punctuation preceding the clause.  |
| clause._.succ_punct | list of Token | Punctuation succeeding the clause. |
| clause._.tokens     | list of Token | All tokens in the clause.          |
| token._.clause      | Span          | The clause containing the token.   |
| **analyzer**        |               |                                    |
| token._.morph       | Morph         | Morphological features object.     |
| **tense_tagger**    |               |                                    |
| clause._.form       | Form          | Clausal features object.           |

`Moph` and `Form` have the following attributes:

| Attribute                | Type          | Values/Description                               |
| ------------------------ | ------------- | ------------------------------------------------ |
| **morphological features** (spaCy) |     |                                                  |
| token._.morph.adp_type   | str           | 'circ', 'post', 'prep'                           |
| token._.morph.aspect     | str           | 'perf'                                           |
| token._.morph.conj_type  | str           | 'comp'                                           |
| token._.morph.foreign    | str           | 'yes'                                            |
| token._.morph.hyph       | str           | 'yes'                                            |
| token._.morph.mood       | str           | 'imp', 'ind'                                     |
| token._.morph.num_type   | str           | 'card'                                           |
| token._.morph.part_type  | str           | 'inf', 'res', 'vbp'                              |
| token._.morph.polarity   | str           | 'neg'                                            |
| token._.morph.poss       | str           | 'yes'                                            |
| token._.morph.pron_type  | str           | 'art', 'dem', 'ind|neg|tot', 'int', 'prs', 'rel' |
| token._.morph.punct_type | str           | 'brck', 'comm', 'peri'                           |
| token._.morph.reflex     | str           | 'yes'                                            |
| token._.morph.verb_form  | str           | 'fin', 'inf', 'part'                             |
| token._.morph.verb_type  | str           | 'mod'                                            |
| **morphological features** (DEMorphy) |  |                                                  |
| token._.morph.case       | str           | 'acc', 'dat', 'gen', 'nom'                       |
| token._.morph.degree     | str           | 'comp', 'pos', 'sup'                             |
| token._.morph.gender     | str           | 'fem', 'masc', 'neut', 'noGender'                |
| token._.morph.inflection | str           | 'inf', 'zu'                                      |
| token._.morph.mode       | str           | 'imp', 'ind', 'subj'                             |
| token._.morph.numerus    | str           | 'sing', 'plu'                                    |
| token._.morph.person     | str           | '1per', '2per', '3per'                           |
| token._.morph.starke     | str           | 'strong', 'weak'                                 |
| token._.morph.tense      | str           | 'past', 'ppast', 'ppres', 'pres'                 |
| **clausal features**     |               |                                                  |
| clause._.form.aspect     | str           | 'imperf', 'perf'                                 |
| clause._.form.mode       | str           | 'imp', 'ind', 'subj:past', 'subj:pres'           |
| clause._.form.tense      | str           | 'fut', 'past', 'pres'                            |
| clause._.form.voice      | str           | 'active', 'pass', 'pass:dynamic', 'pass:static'  |
| clause._.form.verb_form  | str           | 'fin', 'inf', 'part'                             |
| clause._.form.main       | Token         | The main verb of the complex verb form.          |
| clause._.form.modals     | list of Token | All modal verbs of the complex verb form.        |
| clause._.form.verbs      | list of Token | All verbs of the complex verb form.              |

The morphological features from DEMorphy come in two attributes each, e.g. `token._.morph.tense` and `token._.morph.tense_`. The first expression resturns the tense of the token if it is existing and unambiguous; otherwise it returns None. The second expression returns all possible tenses of the token in a set (the set can be empty).

## TLT 2020

To reproduce the results presented at TLT 2020 (see reference below) you have to perform the following steps.

Download the heureCLÉA corpus from [github.com/heureclea](https://github.com/heureclea) and save it to `tense-tagger`:

```
tense-tagger
└── heureclea
    ├── sourcedocuments
    └── time-annotations-compared-public
```

The annotations for the preface of *Don Quijote* are located in `don-quijote-vorrede`.

The directory `tmv-annotator-tool` contains the scripts from the [tmv-annotator](https://github.com/aniramm/tmv-annotator/tree/master/tmv-annotator-tool), the necessary [mate-tools](https://code.google.com/archive/p/mate-tools/downloads) files as well as the output of the tmv-annotator for all texts from heureCLÉA and for *Don Quijote*.

```
tense-tagger
└── tmv-annotator-tool
    ├── .idea                          # (tmv-annotator-tool)
    ├── ger-tagger+lemmatizer
    |    +morphology+graph-based-3.6   # (mate-tools)
    ├── output                         # output for heureCLÉA texts
    ├── temp                           # temporary files
    ├── anna-3.61.jar                  # (mate-tools)
    ├── annotate_files.sh              # main script
    ├── annoTMV.sh                     # (tmv-annotator-tool)
    ├── don-quijote-vorrede.txt.verbs  # output for Don Quijote
    ├── etreVerbs.txt                  # (tmv-annotator-tool)
    ├── README                         # (tmv-annotator-tool)
    ├── seinVerbs.txt                  # (tmv-annotator-tool)
    ├── split_sentences.py             # preprocessing script
    ├── TMV-DE.py                      # (tmv-annotator-tool)
    ├── TMV-EN.py                      # (tmv-annotator-tool)
    ├── TMV-FR.py                      # (tmv-annotator-tool)
    └── TMVtoHTML.py                   # (tmv-annotator-tool)
```

To apply the tmv-annotator to the heureCLÉA texts and *Don Quijote*, navigate into `tmv-annotator-tool`and execute the following commands:

```sh
sh annotate_files.sh ../heureclea/sourcedocuments
sh annotate_files.sh ../don-quijote-vorrede
mv output/don-quijote-vorrede.txt.verbs .
```

The main evaluation script compares the annotations from heureCLÉA and *Don Quijote* with the outputs of the tmv-annotator and the tense-tagger (the output of the tense-tagger is produced within the evaluation):

```sh
python evaluate_tense_tagger.py > evaluate_tense_tagger.txt
```

`evaluate_tense_tagger.txt` contains the numbers reported at TLT 2020. The directory `evaluate_tense_tagger_logs` contains the annotations/outputs for all texts in a juxtaposed form and mapped to a common tagset.

Files starting with `tense_` have five columns:

- token
- POS tag (from spaCy)
- tense label from the annotation
- tense label from tmv-annotator
- tense label from tense-tagger

Files starting with `dq_` (*Don Quijote*) have six columns:

- token
- POS tag (from spaCy)
- finiteness (from gold annotation)
- gold label
- label from tmv-annotator
- label from tense-tagger

The number in the filename denotes tense (`0`), mood (`1`), voice (`2`), modality (`3`), or finiteness (`4`). The suffix `_unmerged` means that the labels from the tense-tagger are not reduced to the (less differentiating) labels of the tmv-annotator.

The evaluation of our clausizer on the CoNLL-2001 shared task test set can be run as follows:

```sh
python evaluate_clausizer.py
```

This will print performances and save a detailed log file `clauses.tsv` in `evaluate_tense_tagger_logs`. The file has four columns:

- token
- gold POS tag
- gold label(s)
- label(s) from tense-tagger

## License/Citation
This work is licensed under a
Creative Commons Attribution 4.0 International License.

If you use this code, you should cite the following paper in your work:

> Tillmann Dönicke (2020). "Clause-Level Tense, Mood, Voice and Modality Tagging for German". In Proceedings of the 19th International Workshop on Treebanks and Linguistic Theories (TLT 2020).

Our analyzer uses morphological features from [DEMorphy](https://github.com/DuyguA/DEMorphy), which is described in:

> Duygu Altinok (2018). "DEMorphy, German Language Morphological Analyzer". arXiv:1803.00902.

The following files in `tmv-annotator-tool` are licensed under a different license:

- the [tmv-annotator](https://github.com/aniramm/tmv-annotator) files are licensed under GNU General Public License v3.0
- the [mate-tools](https://code.google.com/archive/p/mate-tools/) files are licensed under GNU General Public License v3.0
- the output files for [heureCLÉA](https://github.com/heureclea/time-annotations-compared-public) texts, including the temporary files, are licensed under Creative Commons Attribution Share Alike 4.0 International

See [TLT 2020](#tlt-2020) to see which files belong into which group.

The text `don-quijote-vorrede/don-quijote-vorrede.txt` is subject to the terms and conditions of use of [Gutenberg-DE](https://www.projekt-gutenberg.org/info/texte/info.html) which prohibits commercial use without permission.
