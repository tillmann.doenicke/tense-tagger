import os

# This script converts CONLLU files from the UD treebanks into spacy training JSON format.
# Input: ../../../ud-treebanks-v2.6 - The UD treebanks.
# Output: input/.. - JSON files which can be used to train a spacy parser.

version = "ud-treebanks-v2.6"

path = os.path.join("..", "..", "..", version)
lang = "German"
treebanks = ["GSD", "HDT", "LIT", "PUD"]

if os.path.exists(path):

    if not os.path.exists("input"):
        os.mkdir("input")

    for tb in treebanks:
        tb_path = os.path.join(path, "UD_" + lang + "-" + tb)
        for filename in os.listdir(tb_path):
            if filename.endswith(".conllu"):
                os.system("python -m spacy convert " + os.path.join(tb_path, filename) + " input")

else:
    print("Download full (UDT) " + version + " from https://universaldependencies.org/#download !")