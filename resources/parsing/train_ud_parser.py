#!/usr/bin/env python
# coding: utf-8

import json
import networkx as nx
import os
import random
import spacy
import sys
from pathlib import Path
from spacy.gold import GoldParse
from spacy.util import minibatch, compounding


# global variables
TRAIN_DATA = []
TEST_DATA = []


def is_acyclic(heads):
    """Check whether a dependency graph is acyclic.

    Args:
        heads (list of int): Contains for every token the index of its head.
            The index of the root token's head is the index of the root token.
    
    Returns:
        boolean: False if the dependency graph contains cycles; True otherwise.
    
    """
    edges = []
    for i, head in enumerate(heads):
        if i != head: # ROOT
            edges.append((head, i))
    G = nx.DiGraph(edges)
    for cycle in nx.simple_cycles(G):
        return False
    return True


def get_data(dir, files, reduce_subtypes=False, allowed_subtypes=[]):
    """Loads data in spacy training JSON format.

    Args:
        dir (str): The directory with the JSON file(s).
        files (list of str): The list with the filename(s).
        reduce_subtypes (boolean): True iff subtypes (e.g. "acl:relcl") should be reduced to their base types.
        allowed_subtypes (list of str): List of subtypes that should not be reduced.
    
    Returns:
        list of (str,dict of str:(list of obj)): A list of samples. Each sample is a tuple of a sentence as text and a dictionary.
            The dictionary has two key-value pairs; "heads" : a list that contains for every token the index of its head, "deps" : a list that contains for every token the dependency relation.
    
    """
    train_data = []
    for filename in files:
        with open(os.path.join(dir, filename)) as f:
            corpus = json.load(f)
            for document in corpus:
                for paragraph in document["paragraphs"]:
                    for sentence in paragraph["sentences"]:
                        orths = []
                        heads = []
                        deps = []
                        for token in sentence["tokens"]:
                            orth = token["orth"]
                            head = token["id"] + token["head"]
                            dep = token["dep"]
                            if reduce_subtypes and dep not in allowed_subtypes:
                                dep = dep.split(":")[0]
                            orths.append(orth)
                            heads.append(head)
                            deps.append(dep)
                        if is_acyclic(heads):
                            tup = (" ".join(orths), {"heads" : heads, "deps" : deps})
                            train_data.append(tup)
    return train_data


def main(model=None, output_dir=None, n_iter=15):
    """Load the model, set up the pipeline and train the parser.
        Uses TRAIN_DATA and TEST_DATA.
        Source: https://github.com/explosion/spaCy/blob/master/examples/training/train_intent_parser.py
    
    Args:
        model (str): Name of an existing spacy model.
        output_dir (str): Output directory if the trained model should be saved.
        n_iter (int): Number of training iterations.
    
    """
    if model is not None:
        nlp = spacy.load(model)  # load existing spaCy model
        print("Loaded model '%s'" % model)
    else:
        nlp = spacy.blank("de")  # create blank Language class
        print("Created blank 'de' model")

    # We'll use the built-in dependency parser class, but we want to create a
    # fresh instance – just in case.
    if "parser" in nlp.pipe_names:
        nlp.remove_pipe("parser")
    parser = nlp.create_pipe("parser")
    nlp.add_pipe(parser, after="tagger")

    for text, annotations in TRAIN_DATA:
        for dep in annotations.get("deps", []):
            parser.add_label(dep)

    pipe_exceptions = ["parser", "trf_wordpiecer", "trf_tok2vec"]
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe not in pipe_exceptions]
    with nlp.disable_pipes(*other_pipes):  # only train parser
        optimizer = nlp.begin_training()
        a = 0
        e = 0
        for itn in range(n_iter):
            random.shuffle(TRAIN_DATA)
            losses = {}
            # batch up the examples using spaCy's minibatch
            batches = minibatch(TRAIN_DATA, size=compounding(4.0, 32.0, 1.001))
            for batch in batches:
                texts, annotations = zip(*batch)
                try:
                    nlp.update(texts, annotations, sgd=optimizer, losses=losses)
                except:
                    e += 1
                a += 1
            print("Losses", losses)
            print("Errors", 1.0*e/a)

            # test the trained model
            test_model(nlp)

    # save model to output directory
    if output_dir is not None:
        output_dir = Path(output_dir)
        if not output_dir.exists():
            output_dir.mkdir()
        nlp.to_disk(output_dir)
        print("Saved model to", output_dir)

        # test the saved model
        print("Loading from", output_dir)
        nlp2 = spacy.load(output_dir)
        test_model(nlp2)


def test_model(nlp):
    """Test a spacy model.
        Uses TEST_DATA.

    Args:
        nlp (`spacy`): A spacy model.
    
    """
    docs = nlp.pipe([x[0] for x in TEST_DATA])
    a = 0
    c = 0
    e = 0
    for i, doc in enumerate(docs):
        gold = TEST_DATA[i][1]
        if len(doc) != len(gold["deps"]):
            e += len(gold["deps"])
            a += len(gold["deps"])
            continue
        for token in doc:
            a += 1
            if token.head.i == gold["heads"][token.i] and token.dep_ == gold["deps"][token.i]:
                c += 1
    print(1.0*c/a, 1.0*e/a)


def logged_test(train_files, test_files, allowed_subtypes=None, output_dir=None):
    """Train and test a model and save the output to a file.

    Args:
        train_files (list of str): Filenames of the training files.
        test_files (list of str): Filenames of the test files.
        allowed_subtypes (list of str): List of subtypes that should not be reduced.
            If None, subtypes are not reduced. If empty, all subtypes are reduced.
        output_dir (str): Output directory if the trained model should be saved.
    
    """
    global TRAIN_DATA
    global TEST_DATA
    reduce_subtypes = True
    if allowed_subtypes is None:
        allowed_subtypes = []
        reduce_subtypes = False
    train_files = sorted(train_files)
    test_files = sorted(test_files)
    TEST_DATA = get_data("input", test_files, reduce_subtypes, allowed_subtypes)
    TRAIN_DATA = get_data("input", train_files, reduce_subtypes, allowed_subtypes)
    filename = ",".join(train_files) + "+" + ",".join(test_files) + ".txt"
    sout = sys.stdout
    sys.stdout = open(os.path.join("logs", filename),'wt')
    main("de_core_news_lg", output_dir)
    sys.stdout = sout


if __name__ == "__main__":
    common_subtypes = ["aux:pass", "compound:prt", "csubj:pass", "nmod:poss", "nsubj:pass"]
    
    #logged_test(["de_pud-ud-test.json"], ["de_lit-ud-test.json"], common_subtypes)
    #logged_test(["de_pud-ud-test.json", "de_gsd-ud-dev.json", "de_hdt-ud-dev.json"], ["de_lit-ud-test.json"], common_subtypes)
    #logged_test(["de_pud-ud-test.json", "de_gsd-ud-dev.json", "de_hdt-ud-dev.json", "de_gsd-ud-train.json", "de_hdt-ud-train.json"], ["de_lit-ud-test.json"], common_subtypes)
    
    logged_test(["de_pud-ud-test.json", "de_lit-ud-test.json"], ["de_gsd-ud-test.json", "de_hdt-ud-test.json"], common_subtypes, "de_ud_sm")
    logged_test(["de_pud-ud-test.json", "de_lit-ud-test.json", "de_gsd-ud-dev.json", "de_hdt-ud-dev.json"], ["de_gsd-ud-test.json", "de_hdt-ud-test.json"], common_subtypes, "de_ud_md")
    logged_test(["de_pud-ud-test.json", "de_lit-ud-test.json", "de_gsd-ud-dev.json", "de_hdt-ud-dev.json", "de_gsd-ud-train.json", "de_hdt-ud-train.json"], ["de_gsd-ud-test.json", "de_hdt-ud-test.json"], common_subtypes, "de_ud_lg")
    