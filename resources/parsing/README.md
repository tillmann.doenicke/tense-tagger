# Parsing

This directory contains the scripts to train a (new) parser for a German spaCy model on the [Universal Dependencies](https://universaldependencies.org/) as well as the output.

## Download resources

Download and unzip:

- Universal Dependencies 2.6 from [https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3226](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3226); download link: [https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3226/allzip](https://lindat.mff.cuni.cz/repository/xmlui/handle/11234/1-3226/allzip)

Save `ud-treebanks-v2.6` in the same directory as `tense-tagger`.

## Run scripts

Navigate into this directory and run the following commands:

```sh
python convert_ud.py
python train_ud_parser.py
```

## Output

- `de_ud_sm`
- `de_ud_md`
- `de_ud_lg`