import os
import spacy

from settings import PARSING_PATH
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.tense_tagger import rb_tense_tagger
from pipeline.components.pipeline_helper import lemma_fixer


if __name__ == "__main__":
    # load UD spacy model
    sp = spacy.load(os.path.join(PARSING_PATH, "de_ud_lg"))
    
    # add components
    sp.add_pipe(lemma_fixer, name="lemma_fixer", after="tagger")
    sp.add_pipe(spacy_sentencizer, name="sentencizer", before="parser")
    sp.add_pipe(demorphy_analyzer, name="analyzer")
    sp.add_pipe(dependency_clausizer, name="clausizer")
    sp.add_pipe(rb_tense_tagger, name="tense_tagger")

    # small test sentence
    text = "Der Mann, der die ulkige Kuh gesehen haben musste, begann, laut zu lachen."
    doc = sp(text)
    for clause in doc._.clauses:
        print(clause._.tokens)
        print(clause._.form.verbs)
        print(clause._.form.verb_form, clause._.form.tense, clause._.form.aspect, clause._.form.mode, clause._.form.voice, clause._.form.modals)
        print()