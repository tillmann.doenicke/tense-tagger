import os

# path to project root
ROOT_PATH = os.path.dirname(__file__)


# paths to resources for pipeline components

RESOURCES_PATH = os.path.join(ROOT_PATH, "resources")

global FLEXION_PATH
FLEXION_PATH = os.path.join(RESOURCES_PATH, "flexion")

global PARSING_PATH
PARSING_PATH = os.path.join(RESOURCES_PATH, "parsing")

global WIKTIONARY_TOOLS_PATH
WIKTIONARY_TOOLS_PATH = os.path.join(RESOURCES_PATH, "wiktionary_tools")
